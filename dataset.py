from PIL import Image
from os.path import join
from torch.utils.data import Dataset


class ImgDataset(Dataset):
    def __init__(self, files, targets=None, transform=None, base_dir=''):
        self.base_dir = base_dir
        self.files = files
        self.targets = targets 
        self.transform = transform
        
    def __len__(self):
        return len(self.files)
    
    def __getitem__(self, idx):
        filename = self.files[idx]
        img = Image.open(join(self.base_dir, filename))
        
        if self.transform:
            img = self.transform(img)
            
        if self.targets:
            target = self.targets[idx]
            return img, target
        
        return img
