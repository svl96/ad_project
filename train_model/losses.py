import torch
import torch.nn.functional as F


class MAEMultiTarget(torch.nn.Module):
    def __init__(self, num_classes=8):
        super(MAEMultiTarget, self).__init__()
        self.num_classes = num_classes
        self.one_hot = torch.eye(self.num_classes)

    def forward(self, outputs, targets):
        one_hot = torch.eye(self.num_classes).to(targets.device)
        target_one_hot = one_hot.index_select(0, targets)

        return torch.nn.functional.l1_loss(outputs, target_one_hot)


class DistillationLoss(torch.nn.Module):
    def __init__(self, tau=20, alpha=0.5):
        super(DistillationLoss, self).__init__()
        self.tau = tau
        self.alpha = alpha
        self.KLDiv_criterion = torch.nn.KLDivLoss()
        self.cross_entropy = torch.nn.CrossEntropyLoss()

    def forward(self, outputs, targets):
        student_out, teacher_out = outputs

        KLDiv_loss = self.KLDiv_criterion(F.log_softmax(student_out/self.tau),
                                         F.softmax(teacher_out/self.tau)) * (self.tau * self.tau * 2)

        # KLDiv_loss = self.KLDiv_criterion(student_out,
        #                                  teacher_out) * (2)

        cross_entropy_loss = self.cross_entropy(F.log_softmax(student_out), targets)

        return self.alpha * KLDiv_loss + (1 - self.alpha) * cross_entropy_loss


class CombineLoss(torch.nn.Module):
    def __init__(self, device, alpha=0.5, classes=8):
        super(CombineLoss, self).__init__()
        self.alpha = alpha
        self.enc = torch.ones((2, classes), dtype=torch.float)
        self.enc[0, 0] = 1
        self.enc[1, :] = 1 - self.enc[0, :]
        self.enc = self.enc.to(device)

    def forward(self, outputs, targets):
        bin_targets = (targets != 0).long()
        bin_outputs = F.softmax(outputs)
        bin_outputs = torch.stack((bin_outputs[:, 0], bin_outputs[:, 1:].sum(axis=1)), dim=-1)

        return self.alpha * F.cross_entropy(bin_outputs, bin_targets) + (1 - self.alpha) * F.cross_entropy(outputs, targets)
