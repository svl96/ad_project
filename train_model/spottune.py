import torch
import torchvision
from torch import nn
import torch.nn.functional as F


def get_policy_net(out):
    model = torchvision.models.resnet34(pretrained=False)
    model.fc = nn.Linear(model.fc.in_features, out)

    return model


class SpotTuneModel(nn.Module):
    def __init__(self, classes=8):
        super(SpotTuneModel, self).__init__()

        self.model = torchvision.models.resnet50(pretrained=True)
        self.model.fc = nn.Linear(self.model.fc.in_features, classes)


        self.origin = torchvision.models.resnet50(pretrained=True)
        self.origin.requires_grad_(False)

        self.policy = torchvision.models.resnet18(pretrained=False)
        self.policy.fc = nn.Linear(self.policy.fc.in_features, sum([3, 4, 6, 3])*2)


    def forward(self, input):
        rn = self.model
        orn = self.origin

        
        if self.training:
            x = rn.conv1(input)
            x = rn.bn1(x)
            x = rn.relu(x)
            x = rn.maxpool(x)
            probs = self.policy(input)
            policy = (F.gumbel_softmax(probs.view(probs.size(0), -1, 2), hard=True, tau=5))[:, :, 1]

            policy_iter = 0
            for layer, origin_layer in zip(list(rn.children())[4:8], list(orn.children())[4:8]):
                for block, origin_block in zip(layer.children(), origin_layer.children()):
                    action = policy[:, policy_iter].contiguous()
                    action_mask = action.float().view(-1, 1,1,1)

                    output = block(x)
                    origin_output = origin_block(x)

                    x = output * (1 - action_mask) + origin_output * action_mask
                    policy_iter += 1

            x = rn.avgpool(x)
            x = torch.flatten(x, 1)
            x = rn.fc(x)
        else:
            x = rn(input)

        return x


class SpotTuneOptimizer:
    def __init__(self, model_param, agent_param, args):
        self.optimizer = torch.optim.SGD(model_param, lr=args.opt_lr,
                                    momentum=0.9, weight_decay=0.0001)
        self.agent_optimizer = torch.optim.SGD(agent_param, lr=args.agent_lr,
                                        momentum= 0.9, weight_decay= 0.001)

    def step(self):
        self.optimizer.step()
        self.agent_optimizer.step()

    def zero_grad(self):
        self.optimizer.zero_grad()
        self.agent_optimizer.zero_grad()
