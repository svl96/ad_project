import torch
import torchvision
from torchvision import transforms
from torch.utils.data import DataLoader
from dataset import ImgDataset
from torch.utils.checkpoint import checkpoint_sequential

import logging 
import argparse
import numpy as np
from datetime import datetime
from cls_models import get_model

# from utils import TimeProfiler, AccMultWrapper, plot_train_curve, plot_test_res, get_test_data, load_data, split_train_test_val
# from utils import DistillationAccuracy, DistillationLoss, TrainLogger
from utils.profiler import TimeProfiler
from utils.logger import TrainLogger
from utils.data import split_train_test_val, load_data
from utils.plots import plot_train_curve, plot_test_res
from metrics import AccMultWrapper
from losses import CombineLoss


EPOCH_TIMER = 'epoch_timer'
BATCH_TIMER = 'batch_timer'
BASE_DIR = 'data'

device = torch.device("cuda:0" if torch.cuda.is_available() else 'cpu')
profiler = TimeProfiler()
ts = datetime.now().strftime("%Y-%m-%d %H.%M.%S")
logger = TrainLogger(logging, profiler)


def activate_random_layers(model, args):
    params = list(model.parameters())[-10:]
    for param in params:
        param.requires_grad = False

    for i in np.random.choice(len(params), size=10):
        params[i].requires_grad = True


def train_one_epoch(model, dataloader, criterion, optimizer, metric, args):
    metric.reset('train')
    total_loss = []
    model.train(True)
    optimizer.zero_grad()

    profiler.start_timer(BATCH_TIMER)
    step = 100
    
    for i, (samples, targets) in enumerate(dataloader):
        samples = samples.to(device)
        targets = targets.to(device)
        
        if args.activate_random:
            activate_random_layers(model, args)

        if args.checkpoint > 0:
            outputs = checkpoint_sequential(model, args.checkpoint, samples)
        else:
            outputs = model(samples)
        loss = criterion(outputs, targets)
        metric(outputs, targets, mode='train')

        loss.backward()
    
        if args.batch_count <= 1 or (i+1) % args.batch_count == 0:
            optimizer.step()
            optimizer.zero_grad()

        total_loss.append(loss.item())
        profiler.loop_timer(BATCH_TIMER)

        if (i+1) % step == 0:
            logger.batch_step(i+1, np.mean(total_loss), metric)

    return sum(total_loss) / len(total_loss)


@torch.no_grad()
def eval_model(model, dataloader, criterion, metric):
    total_loss = []
    metric.reset('test')
    model.eval()

    for i, (samples, targets) in enumerate(dataloader):
        samples = samples.to(device)
        targets = targets.to(device)

        outputs = model(samples)
        total_loss.append(criterion(outputs, targets).item())
        metric(outputs, targets, mode='test')

    return sum(total_loss) / len(total_loss)


def set_require_grad(model, count, args):
    # if args.model == 'dense121':
    #     count += 2
    
    for param in list(model.parameters())[-count:]:
        param.requires_grad = True

class FilterScheduler:
    def __init__(self, steps, layers):
        self.steps = steps
        self.layers = layers 
        self.i = 0
        self.pos = 0
        assert len(self.layers) == len(self.steps)

    def set_require_grad(self, model, layer_num):
        for layer in list(model.resnet.children())[layer_num:]:
            layer.requires_grad_(True)
    

    def step(self, model):
        self.pos += 1
        if self.i == len(self.steps):
            return

        if self.pos == self.steps[self.i]:
            logging.info('FilterScheduler %d', self.layers[self.i])
            self.set_require_grad(model, self.layers[self.i])
            self.i += 1


def train_model(model, dataloaders, scheduler, criterion, optimizer, metric, args):
    loss_hist = {'train': [], 'test': []}
    epochs = args.epochs
    profiler.start_timer(EPOCH_TIMER)
    count = 10
    max_val_acc = 0
    # if not args.adaptive and args.model != 'spottune':
    #     set_require_grad(model, count, args)

    # logging.info("Count %d Random %d " % (count, 10))
    # filter_scheduler = FilterScheduler([1, 4, 6], [7, 6, 5])
    # logging.info('FilterScheduler([1, 4, 6], [7, 6, 5])')

    for epoch in range(epochs):
        loss = train_one_epoch(model, dataloaders['train'], criterion,
                                    optimizer, metric, args)
        
        val_loss = eval_model(model, dataloaders['val'], criterion, metric)

        logger.train_step(epoch + 1, epochs, metric, loss, val_loss)
            
        loss_hist['train'].append(loss)
        loss_hist['test'].append(val_loss)

        if metric.get_mean_bin('test') > max_val_acc:
            max_val_acc = metric.get_mean_bin('test') 
            if args.out and args.save_best:
                torch.save(model.state_dict(), args.out)
        
        if args.adaptive: # and ((epoch + 1) % 2 == 0):
            count += 5
            # logging.info('Count: {}'.format(count))
            # scheduler.step()
            # filter_scheduler.step(model)
            # set_require_grad(model, count, args)

    return model, loss_hist

CLASSES = ['good', 'celebrity', 'casino', 'guns',
            'illegal', 'narcotics', 'porno', 'yellow']


def get_dataloaders(batch_size=20, val_size=0.2,
                    num_workers=4, base_dir=BASE_DIR, 
                    shuffle=True, classes=CLASSES):
    logging.info("Batch size %d" %batch_size)
    # size = (299, 299)
    size = (224, 224)
    data_transform = {
        "train": transforms.Compose([
                transforms.Resize(size),
                transforms.RandomAffine(degrees=15, translate=(0.1, 0.1), scale=(0.9, 1.1)),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                ]),
        "test": transforms.Compose([
                transforms.Resize(size),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                ])
    }
    
    logging.info("Classes: %s" % ' '.join(classes))

    X, y = load_data(classes=classes, base_dir=base_dir)
    train, val, test = split_train_test_val(X, y, val_size=val_size, random_state=42)
    # narco = list(np.array(train[0])[np.array(train[1]) == 5])
    # train = (train[0] + narco, train[1] + [5] * len(narco))
    # logging.info('Added narco x2')

    datasets = {
        "train": ImgDataset(train[0], targets=train[1], transform=data_transform['train']),
        "val": ImgDataset(val[0], targets=val[1], transform=data_transform['test']),
        "test": ImgDataset(test[0], targets=test[1], transform=data_transform['test']),
        }
    
    dataloaders = {
        "train": DataLoader(datasets['train'], shuffle=shuffle, 
                            batch_size=batch_size, num_workers=num_workers),
        "val": DataLoader(datasets['val'], shuffle=False, 
                            batch_size=batch_size, num_workers=num_workers),
        "test": DataLoader(datasets['test'], shuffle=False, 
                            batch_size=batch_size, num_workers=num_workers),
        }

    return dataloaders, (train, val, test)

from spottune import SpotTuneOptimizer

def run_training(model, args):
    
    classes = ['good', 'celeb', 'casino', 'guns',
                'illegal', 'narco', 'porno', 'yellow']

    logging.info("device %s" %device)
    profiler.reset()
    model = model.to(device)
    logging.info("Adaptive")
    
    metric = AccMultWrapper()
    weights = torch.as_tensor([0.5, 1, 1, 1, 1, 1, 1, 0.5], dtype=torch.float32)
    weights = weights.to(device)
    criterion = torch.nn.CrossEntropyLoss(weight=weights)
    # logging.info('Combine loss alpha=0.9')
    logging.info('Weights [0.5, 1, 1, 1, 1, 1, 1, 0.5]')

    # criterion = CombineLoss(device, classes=len(classes), alpha=0.9)

    if args.model == 'spottune':
        optimizer = SpotTuneOptimizer(model.model.parameters(), model.policy.parameters(), args)
        logging.info('Optim SpotTune')
    else:
        optimizer = torch.optim.Adam(model.parameters(), lr=args.opt_lr)
        logging.info('Optim Adam')
    # optimizer = torch.optim.SGD(model.parameters(), lr=args.opt_lr)

    # scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[1, 4, 6], gamma=0.1)
    # logging.info('LR: {} Milestone [1, 4, 6, ...] Gamma 0.1'.format(args.opt_lr))
    scheduler = None
    # scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.5)
    # logging.info('LR: {} 0.5'.format(args.opt_lr))


    dataloaders, data = get_dataloaders(batch_size=args.batch, 
                        num_workers=args.num_workers,
                        base_dir=args.data_dir,
                        classes=classes)
    
    logging.info('Run training\n')
    model, loss_hist = train_model(model, dataloaders, scheduler, criterion,
                                             optimizer, metric, args)
    # if args.out != '':
    #     torch.save(model.state_dict(), args.out)
    if args.out and args.save_best:
        model.load_state_dict(torch.load(args.out))
    # logging.info("\nTEST Loss: {:.4f}; Accuracy: {:.4f}\n".format(loss_test, acc_test))

    loss_test = eval_model(model, dataloaders['test'], criterion, metric)
    logger.test_step(loss_test, metric)
    
    if args.plot_hist:
        import os
        dir_name = 'plots/train_curve_{}'.format(ts)
        if not os.path.isdir(dir_name):
            os.mkdir(dir_name)
        plot_train_curve(loss_hist, metric, dir_name=dir_name)
        plot_test_res(model, data, dataloaders, classes, device, logging, dir_name=dir_name)

    
    return model, loss_test


def load_model(path):
    model = get_model('resnet50')
    model.load_state_dict(torch.load(path))
    model.eval()

    return model


def get_args():
    parser = argparse.ArgumentParser(description='Image Classifier training')
    parser.add_argument('--data_dir', type=str, default='data', help='directory for image')
    parser.add_argument('--model', type=str, default='resnet50', help='model name to train, resnet/big/standard/distill')
    parser.add_argument('--out', type=str, default='', help='File to save model')
    parser.add_argument('--epochs', type=int, default=10, help='Number of epochs for training')
    parser.add_argument('--batch', type=int, default=50, help='Size of Batch')
    parser.add_argument('--checkpoint', type=int, default=-1, help='Size of checkpoint')
    parser.add_argument('--opt_lr', type=float, default=0.001, help='Learning rate for optimizer')
    parser.add_argument('--agent_lr', type=float, default=0.01, help='Learning rate for optimizer for agent')
    parser.add_argument("--batch_count", type=int, default=1, help='Multiplier for computational batch size equals "total=batch_count*batch"')
    parser.add_argument('--plot_hist', action='store_false', default=True, help='Plot history of training')
    parser.add_argument('--memory_usage',  action='store_false', default=True, help='Show memory usage')
    parser.add_argument('--no_scheduler',  action='store_true', default=False, help='Dont Apply scheduler')
    parser.add_argument('--activate_random',  action='store_true', default=False, help='Activate random layers')
    parser.add_argument('--num_workers', type=int, default=4, help='Number of workers for dataloader')
    parser.add_argument('--stop_accuracy', type=float, default=0.4, help='Accuracy for early stopping')
    parser.add_argument('--save_best', action='store_false', default=True, help='Save model with better result')
    parser.add_argument('--adaptive', action='store_false', default=True, help='Enable Adaprive learning with scheduler')
    parser.add_argument('--teacher_file', type=str, default='', help='state dict for teacher model for distill')
    parser.add_argument('--model_state', type=str, default='', help='start state dict for model to train')


    return parser.parse_args()


def check_dirs():
    import os
    dirs = ['models', 'plots', 'logs']
    for dirr in dirs:
        if not os.path.isdir(dirr):
            os.mkdir(dirr)


def main():
    # timestamp = datetime.now().strftime("%Y-%m-%d %H.%M.%S")
    check_dirs()
    logfile = "logs/log_%s.txt" % ts
    logging.basicConfig(filename=logfile, level=logging.INFO, format=u'%(message)s')
    logging.info("Logging Timestamp %s\n" % ts)
    args = get_args()
    profiler.reset()
    if args.out == '':
        args.out = 'models/model_%s_%s.pth' % (args.model, ts)
    logging.info(args)
    logging.info('Model %s require grad = False' % (args.model))
    model = get_model(args.model, teacher_file=args.teacher_file) if args.model=='distill' else get_model(args.model)
    
    if args.model_state:
        model.load_state_dict(torch.load(args.model_state))

    run_training(model, args) 
    if args.memory_usage:
        logging.info(f"Peak memory usage by Pytorch tensors: {(torch.cuda.max_memory_allocated() / 1024 / 1024):.2f} Mb")

    with open(logfile, 'r') as f:
        print(f.read())


if __name__ == '__main__':
    main()
