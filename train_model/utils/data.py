import os 
import torch
from sklearn.model_selection import train_test_split 


def get_test_data(ids_file, annotation_file):
        
    with open(ids_file, 'r') as f:
        ids = f.readlines()
    ids_map = {val: i for i, val in enumerate(sorted([val.strip() for val in ids]))}    
    
    with open(annotation_file, 'r') as f:
        annotations = f.readlines()
    data = [row.split('\t')[:2] for row in annotations]
    
    filenames, targets = [], []
    for fn, tr in data:
        filenames.append(fn)
        targets.append(ids_map[tr])
    
    return filenames, targets


def load_data(classes=None, base_dir='data', bad_files=set()):
    # classes = {'good': 0, 'bad/casino': 1,
    # 'bad/celebrity': 2, 'bad/guns': 3,
    # 'bad/illegal': 4, 'bad/narcotics': 5,
    # 'bad/porno': 6, 'bad/yellow': 7}
    
    classes = classes or ['good', 'celebrity', 'casino', 'guns',
                'illegal', 'narcotics', 'porno', 'yellow']

    # classes = ['good', 'porno', 'yellow']

    X = []
    y = []
    for v, key in enumerate(classes):
        for el in os.listdir(os.path.join(base_dir, key)):
            img_path = os.path.join(base_dir, key, el)
            if img_path in bad_files:
                continue
            X.append(img_path)
            y.append(v)
    
    return (X, y)


def split_train_test_val(X, y, test_size=0.1, val_size=0.1, shuffle=True, random_state=42):
    X_train, X_test, y_train, y_test = train_test_split(X, y,
                                    test_size=test_size, shuffle=shuffle, stratify=y, random_state=random_state)

    X_train, X_val, y_train, y_val = train_test_split(X_train, y_train,
                                    test_size=val_size, shuffle=shuffle, stratify=y_train, random_state=random_state)

    return (X_train, y_train), (X_val, y_val), (X_test, y_test)


def get_preds(model, dataloader, device):
    model.eval()
    all_preds = []
    all_probs = []
    for i, (samples, targets) in enumerate(dataloader):
        samples = samples.to(device)
        targets = targets.to(device)

        outputs = model(samples)
        output_softmax = torch.nn.Softmax()(outputs)
        # print(output_softmax.c)
        # break
        probs, preds = torch.max(output_softmax, 1)
        all_preds += list(preds.cpu().numpy())
        all_probs += list(probs.detach().cpu().numpy())
        # print(acc_multi, acc_bin)

    return all_preds, all_probs
