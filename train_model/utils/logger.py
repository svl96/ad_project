

EPOCH_TIMER = 'epoch_timer'
BATCH_TIMER = 'batch_timer'


class TrainLogger:
    def __init__(self, logger, profiler):
        self.logger = logger
        self.profiler = profiler

    def train_step(self, epoch, epochs, metric, loss, val_loss):
        self.logger.info("Epoch [{}/{}] Time: {:.2f}s; BatchTime:{:.2f}s; Loss: {:.4f}; MultAcc: {:.4f}; BinAcc: {:.4f}; \n \t \t ValLoss: {:.4f}; ValMultAcc: {:.4f}; ValBinAcc: {:.4f}".format(
                epoch, epochs, 
                self.profiler.loop_timer(EPOCH_TIMER),
                self.profiler.get_mean(BATCH_TIMER), 
                loss, metric.get_mean_mult('train'), metric.get_mean_bin('train'),
                val_loss, metric.get_mean_mult('test'), metric.get_mean_bin('test')))

    def test_step(self, loss, metric):
        self.logger.info("\nTEST Loss: {:.4f}; AccMult: {:.4f}; AccBin: {:.4f}\n".format(
                            loss, metric.get_mean_mult('test'),
                            metric.get_mean_bin('test')))

    def batch_step(self, step, loss, metric):
        self.logger.info("Step: {}; {:.2f}.s Loss: {:.4f}; AccMul {:.4f} AccBin {:.4f}".format(step, 
                self.profiler.get_mean(BATCH_TIMER) * step,
                loss, metric.get_mean_mult('train'),
                 metric.get_mean_bin('train')))

