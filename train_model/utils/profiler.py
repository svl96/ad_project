from time import time


class TimeProfiler:
    def __init__(self):
        self.timers = {}
        self.timer_step = {}

    def reset(self):
        self.timers = {}
        self.timer_step = {}

    def has_timer(self, name):
        return (name in self.timers) and (name in self.timer_step)
    
    def start_timer(self, name):
        if not self.has_timer(name):
            self.timers[name] = []
        
        self.timer_step[name] = time()

    def loop_timer(self, name):
        if not self.has_timer(name):
            self.start_timer()
            return
        
        self.timers[name].append(time() - self.timer_step[name])
        self.timer_step[name] = time()
    
        return self.timers[name][-1]
    
    def get_mean(self, name):
        if (not self.has_timer(name)) or (len(self.timers[name]) == 0):
            return 0
        
        return sum(self.timers[name]) / len(self.timers[name])

