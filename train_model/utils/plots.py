import os
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import roc_auc_score, roc_curve, confusion_matrix
from sklearn.utils.multiclass import unique_labels
from .data import get_preds


def save_roc(preds_all, probs_all, targets, classes,
                logger, filename='roc_auc.png'):
    # classes = np.asarray(classes)
    plt.title('ROC Curve')
    plt.grid()
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    logger.info('ROC AUC SCORE')
    for target, label in enumerate(classes):
        preds = (np.asarray(preds_all) != target).astype('int')
        probs = np.asarray(probs_all)
        probs_bin = np.abs(probs - (1 - preds))
        bin_target = (np.asarray(targets) != target).astype('int')
        logger.info("\t {}: {:.4f}".format(label, roc_auc_score(bin_target, probs_bin)))
        fpr, tpr, thresholds = roc_curve(bin_target, probs_bin)

        plt.plot(fpr, tpr, label=label)
        plt.legend()
    
    plt.savefig(filename)
    plt.show()


def plot_hist(hist, title, ylabel, xlabel='Epochs', filename='plot.png'):
    plt.title(title)
    plt.grid()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.plot(hist['train'], label='train')
    plt.plot(hist['test'], label='val')
    plt.legend()
    plt.savefig(filename)
    plt.show()


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues,
                          filename='conf_matrix.png'):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'
    classes = np.asarray(classes)
    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        # print("Normalized confusion matrix")
    # else:
        # print('Confusion matrix, without normalization')

    # print(cm)
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

#     # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    bottom, top = ax.get_ylim()
    ax.set_ylim(bottom + 0.5, top - 0.5)
    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    plt.savefig(filename)
    plt.show()



def plot_train_curve(loss_hist, metric, dir_name='plots/train_results'):

    plot_hist(loss_hist, "Loss History", 'CrossEntropyLoss',
                filename=os.path.join(dir_name, 'loss_hist.png'))
    
    plot_hist(metric.hist_mult, "Multiclass Accuracy History", 'Accuracy',
                filename=os.path.join(dir_name, 'acc_mult_hist.png'))

    plot_hist(metric.hist_bin, "Binary Accuracy History", 'Accuracy', 
                filename=os.path.join(dir_name, 'acc_bin_hist.png'))


def plot_test_res(model, data, dataloaders, classes,
                    device, logger, dir_name='plots/train_results'):
    train, val, test = data
    preds_all, probs_all = get_preds(model, dataloaders['test'], device)
    targets = test[1]

    save_roc(preds_all, probs_all, targets, classes, logger, 
            filename=os.path.join(dir_name, 'roc_curve.png'))

    plot_confusion_matrix(targets, preds_all, classes, normalize=True,
                filename=os.path.join(dir_name, 'conf_matrix.png'))

