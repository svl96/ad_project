
import argparse
import pandas as pd 
from os import path


def run(args):
    df = pd.read_csv(args.input, sep='\t')
    mean_by_url = df.groupby(by=['id', 'img_url']).mean()
    mm = (mean_by_url[(mean_by_url['target'] < 0.05) | (mean_by_url['target'] > 0.95)] > 0.5).astype('int')
    mm.reset_index(inplace=True)

    urls = []
    class_labels = [] 
    bad_target = ['casino', 'celeb', 'guns', 'illegal', 'narco', 'porno', 'yellow']
    for i, row in mm.iterrows():
        if sum(row[2:-1]) > 1:
            continue
        if row['target'] == 0:
            urls.append(row['img_url'])
            class_labels.append('good')
        else:
            for (cl, val) in zip(bad_target, row[2:-1]):
                if val == 1:
                    urls.append(row['img_url'])
                    class_labels.append(cl)
                    break
    
    res = pd.DataFrame({'url': urls, 'class_label': class_labels, 'dataset_part': ['data'] * len(urls)})
    res.to_csv(args.out, index=False, sep='\t')


def get_args():
    parser = argparse.ArgumentParser(description="Image loader")
    parser.add_argument("input", type=str, help='input table')
    parser.add_argument('out', type=str, help='output table')

    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    run(args)