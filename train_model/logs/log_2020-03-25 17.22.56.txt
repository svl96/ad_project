Logging Timestamp 2020-03-25 17.22.56

Namespace(batch=100, batch_count=1, checkpoint=-1, data_dir='data', epochs=12, memory_usage=True, model='resnet50', no_scheduler=False, num_workers=4, opt_lr=0.01, out='models/model_resnet50_2020-03-25 17.22.56.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model require grad = False
device cuda:0
Adaptive
Combine loss alpha=0.8
Batch size 100
Classes: good celebrity casino guns illegal narcotics porno yellow
Run training

Step: 100; 67.09.s Loss: 0.7606; AccMul 0.6664 AccBin 0.7906
Step: 200; 122.49.s Loss: 0.6753; AccMul 0.6952 AccBin 0.8080
Step: 300; 178.03.s Loss: 0.6433; AccMul 0.7057 AccBin 0.8159
Step: 400; 233.54.s Loss: 0.6278; AccMul 0.7133 AccBin 0.8203
Step: 500; 287.79.s Loss: 0.6204; AccMul 0.7163 AccBin 0.8237
Epoch [1/12] Time: 382.86s; BatchTime:0.57s; Loss: 0.6203; MultAcc: 0.7172; BinAcc: 0.8235; 
 	 	 ValLoss: 0.5710; ValMultAcc: 0.7324; ValBinAcc: 0.8461
Learning rate: [0.01]; Count: 5
Step: 100; 58.67.s Loss: 0.6407; AccMul 0.7225 AccBin 0.8266
Step: 200; 116.73.s Loss: 0.5922; AccMul 0.7347 AccBin 0.8350
Step: 300; 174.38.s Loss: 0.5707; AccMul 0.7427 AccBin 0.8399
Step: 400; 231.74.s Loss: 0.5592; AccMul 0.7473 AccBin 0.8423
Step: 500; 288.96.s Loss: 0.5516; AccMul 0.7517 AccBin 0.8441
Epoch [2/12] Time: 385.96s; BatchTime:0.58s; Loss: 0.5495; MultAcc: 0.7527; BinAcc: 0.8445; 
 	 	 ValLoss: 0.5455; ValMultAcc: 0.7680; ValBinAcc: 0.8199
Learning rate: [0.0025]; Count: 10
Step: 100; 58.53.s Loss: 0.5171; AccMul 0.7731 AccBin 0.8500
Step: 200; 116.95.s Loss: 0.5085; AccMul 0.7763 AccBin 0.8536
Step: 300; 175.29.s Loss: 0.5044; AccMul 0.7794 AccBin 0.8556
Step: 400; 233.57.s Loss: 0.4999; AccMul 0.7820 AccBin 0.8589
Step: 500; 291.80.s Loss: 0.4982; AccMul 0.7831 AccBin 0.8596
Epoch [3/12] Time: 393.62s; BatchTime:0.58s; Loss: 0.4987; MultAcc: 0.7829; BinAcc: 0.8593; 
 	 	 ValLoss: 0.4870; ValMultAcc: 0.7895; ValBinAcc: 0.8710
Learning rate: [0.00125]; Count: 15
Step: 100; 58.96.s Loss: 0.5095; AccMul 0.7699 AccBin 0.8540
Step: 200; 118.06.s Loss: 0.5024; AccMul 0.7791 AccBin 0.8583
Step: 300; 177.26.s Loss: 0.4991; AccMul 0.7816 AccBin 0.8598
Step: 400; 236.58.s Loss: 0.4968; AccMul 0.7831 AccBin 0.8610
Step: 500; 295.95.s Loss: 0.4939; AccMul 0.7853 AccBin 0.8628
Epoch [4/12] Time: 405.59s; BatchTime:0.59s; Loss: 0.4930; MultAcc: 0.7862; BinAcc: 0.8634; 
 	 	 ValLoss: 0.4812; ValMultAcc: 0.7992; ValBinAcc: 0.8691
Learning rate: [0.000625]; Count: 20
Step: 100; 59.70.s Loss: 0.4854; AccMul 0.7952 AccBin 0.8681
Step: 200; 119.59.s Loss: 0.4830; AccMul 0.7947 AccBin 0.8684
Step: 300; 179.64.s Loss: 0.4798; AccMul 0.7983 AccBin 0.8706
Step: 400; 239.84.s Loss: 0.4776; AccMul 0.7997 AccBin 0.8715
Step: 500; 300.13.s Loss: 0.4754; AccMul 0.8019 AccBin 0.8725
Epoch [5/12] Time: 410.48s; BatchTime:0.60s; Loss: 0.4747; MultAcc: 0.8024; BinAcc: 0.8732; 
 	 	 ValLoss: 0.4718; ValMultAcc: 0.8048; ValBinAcc: 0.8756
Learning rate: [0.0003125]; Count: 25
Step: 100; 60.37.s Loss: 0.4466; AccMul 0.8246 AccBin 0.8940
Step: 200; 120.73.s Loss: 0.4511; AccMul 0.8218 AccBin 0.8901
Step: 300; 181.06.s Loss: 0.4489; AccMul 0.8236 AccBin 0.8912
Step: 400; 241.37.s Loss: 0.4494; AccMul 0.8239 AccBin 0.8911
Step: 500; 301.66.s Loss: 0.4489; AccMul 0.8247 AccBin 0.8909
Epoch [6/12] Time: 400.19s; BatchTime:0.60s; Loss: 0.4488; MultAcc: 0.8249; BinAcc: 0.8913; 
 	 	 ValLoss: 0.4636; ValMultAcc: 0.8167; ValBinAcc: 0.8868
Learning rate: [0.00015625]; Count: 30
Step: 100; 60.69.s Loss: 0.4296; AccMul 0.8460 AccBin 0.9040
Step: 200; 121.55.s Loss: 0.4271; AccMul 0.8457 AccBin 0.9054
Step: 300; 182.57.s Loss: 0.4290; AccMul 0.8426 AccBin 0.9037
Step: 400; 243.75.s Loss: 0.4294; AccMul 0.8422 AccBin 0.9039
Step: 500; 305.07.s Loss: 0.4300; AccMul 0.8418 AccBin 0.9036
Epoch [7/12] Time: 419.30s; BatchTime:0.61s; Loss: 0.4305; MultAcc: 0.8410; BinAcc: 0.9029; 
 	 	 ValLoss: 0.4602; ValMultAcc: 0.8221; ValBinAcc: 0.8855
Learning rate: [7.8125e-05]; Count: 35
Step: 100; 61.46.s Loss: 0.3983; AccMul 0.8702 AccBin 0.9243
Step: 200; 123.31.s Loss: 0.4031; AccMul 0.8634 AccBin 0.9199
Step: 300; 185.68.s Loss: 0.4019; AccMul 0.8648 AccBin 0.9204
Step: 400; 248.44.s Loss: 0.4008; AccMul 0.8668 AccBin 0.9220
Step: 500; 311.35.s Loss: 0.4011; AccMul 0.8661 AccBin 0.9218
Epoch [8/12] Time: 454.11s; BatchTime:0.62s; Loss: 0.4006; MultAcc: 0.8666; BinAcc: 0.9224; 
 	 	 ValLoss: 0.4675; ValMultAcc: 0.8171; ValBinAcc: 0.8860
Learning rate: [3.90625e-05]; Count: 40
Step: 100; 62.74.s Loss: 0.3869; AccMul 0.8779 AccBin 0.9312
Step: 200; 125.86.s Loss: 0.3800; AccMul 0.8851 AccBin 0.9362
Step: 300; 188.96.s Loss: 0.3779; AccMul 0.8871 AccBin 0.9384
Step: 400; 252.14.s Loss: 0.3758; AccMul 0.8893 AccBin 0.9391
Step: 500; 315.41.s Loss: 0.3746; AccMul 0.8897 AccBin 0.9393
Epoch [9/12] Time: 439.12s; BatchTime:0.63s; Loss: 0.3750; MultAcc: 0.8893; BinAcc: 0.9394; 
 	 	 ValLoss: 0.4683; ValMultAcc: 0.8253; ValBinAcc: 0.8911
Learning rate: [1.953125e-05]; Count: 45
Step: 100; 63.35.s Loss: 0.3557; AccMul 0.9044 AccBin 0.9521
Step: 200; 126.98.s Loss: 0.3546; AccMul 0.9049 AccBin 0.9516
Step: 300; 190.98.s Loss: 0.3549; AccMul 0.9046 AccBin 0.9518
Step: 400; 255.25.s Loss: 0.3560; AccMul 0.9041 AccBin 0.9503
Step: 500; 319.82.s Loss: 0.3564; AccMul 0.9029 AccBin 0.9497
Epoch [10/12] Time: 456.24s; BatchTime:0.64s; Loss: 0.3566; MultAcc: 0.9025; BinAcc: 0.9496; 
 	 	 ValLoss: 0.4710; ValMultAcc: 0.8293; ValBinAcc: 0.8902
Learning rate: [9.765625e-06]; Count: 50
Step: 100; 64.35.s Loss: 0.3431; AccMul 0.9164 AccBin 0.9588
Step: 200; 129.16.s Loss: 0.3462; AccMul 0.9134 AccBin 0.9569
Step: 300; 194.38.s Loss: 0.3460; AccMul 0.9140 AccBin 0.9568
Step: 400; 259.89.s Loss: 0.3463; AccMul 0.9133 AccBin 0.9567
Step: 500; 325.58.s Loss: 0.3468; AccMul 0.9119 AccBin 0.9565
Epoch [11/12] Time: 480.00s; BatchTime:0.65s; Loss: 0.3469; MultAcc: 0.9117; BinAcc: 0.9560; 
 	 	 ValLoss: 0.4762; ValMultAcc: 0.8280; ValBinAcc: 0.8903
Learning rate: [4.8828125e-06]; Count: 55
Step: 100; 65.48.s Loss: 0.3429; AccMul 0.9152 AccBin 0.9580
Step: 200; 131.17.s Loss: 0.3410; AccMul 0.9161 AccBin 0.9590
Step: 300; 196.91.s Loss: 0.3420; AccMul 0.9149 AccBin 0.9586
Step: 400; 262.77.s Loss: 0.3410; AccMul 0.9157 AccBin 0.9593
Step: 500; 328.89.s Loss: 0.3401; AccMul 0.9167 AccBin 0.9595
Epoch [12/12] Time: 461.55s; BatchTime:0.66s; Loss: 0.3406; MultAcc: 0.9159; BinAcc: 0.9588; 
 	 	 ValLoss: 0.4773; ValMultAcc: 0.8264; ValBinAcc: 0.8879
Learning rate: [2.44140625e-06]; Count: 60

TEST Loss: 0.4705; AccMult: 0.8261; AccBin: 0.8898

ROC AUC SCORE
	 good: 0.9404
	 celebrity: 0.9714
	 casino: 0.9143
	 guns: 0.9435
	 illegal: 0.8855
	 narcotics: 0.8095
	 porno: 0.9585
	 yellow: 0.8558
Peak memory usage by Pytorch tensors: 2932.27 Mb
