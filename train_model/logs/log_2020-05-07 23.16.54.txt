Logging Timestamp 2020-05-07 23.16.54

Namespace(activate_random=False, adaptive=False, batch=50, batch_count=1, checkpoint=-1, data_dir='data_new', epochs=1, memory_usage=True, model='filtered', no_scheduler=False, num_workers=4, opt_lr=0.001, out='models/model_filtered_2020-05-07 23.16.54.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model filtered require grad = False
device cuda:0
Adaptive
Weights [0.5, 1, 1, 1, 1, 1, 1, 0.5]
Optim Adam
LR: 0.001 0.5
Batch size 50
Classes: good celeb casino guns illegal narco porno yellow
Run training

Count 10 Random 10 
Step: 100; 67.01.s Loss: 1.4509; AccMul 0.5276 AccBin 0.7268
Step: 200; 123.61.s Loss: 1.3742; AccMul 0.5540 AccBin 0.7437
Step: 300; 180.39.s Loss: 1.3385; AccMul 0.5669 AccBin 0.7544
Step: 400; 237.30.s Loss: 1.3169; AccMul 0.5724 AccBin 0.7608
Step: 500; 294.19.s Loss: 1.3063; AccMul 0.5760 AccBin 0.7638
Step: 600; 351.29.s Loss: 1.3009; AccMul 0.5776 AccBin 0.7657
Step: 700; 408.29.s Loss: 1.2924; AccMul 0.5799 AccBin 0.7655
Step: 800; 465.23.s Loss: 1.2833; AccMul 0.5829 AccBin 0.7660
Step: 900; 522.25.s Loss: 1.2741; AccMul 0.5862 AccBin 0.7682
Step: 1000; 579.38.s Loss: 1.2661; AccMul 0.5883 AccBin 0.7690
Step: 1100; 636.47.s Loss: 1.2597; AccMul 0.5902 AccBin 0.7707
Step: 1200; 693.55.s Loss: 1.2558; AccMul 0.5919 AccBin 0.7719
Step: 1300; 750.61.s Loss: 1.2500; AccMul 0.5936 AccBin 0.7737
Step: 1400; 807.58.s Loss: 1.2456; AccMul 0.5949 AccBin 0.7739
Step: 1500; 864.54.s Loss: 1.2398; AccMul 0.5964 AccBin 0.7750
Step: 1600; 921.57.s Loss: 1.2355; AccMul 0.5976 AccBin 0.7752
Step: 1700; 978.61.s Loss: 1.2314; AccMul 0.5989 AccBin 0.7764
Step: 1800; 1035.66.s Loss: 1.2274; AccMul 0.6004 AccBin 0.7773
Step: 1900; 1092.81.s Loss: 1.2237; AccMul 0.6013 AccBin 0.7773
Step: 2000; 1150.62.s Loss: 1.2205; AccMul 0.6021 AccBin 0.7777
Step: 2100; 1209.42.s Loss: 1.2177; AccMul 0.6031 AccBin 0.7783
Epoch [1/1] Time: 1362.22s; BatchTime:0.58s; Loss: 1.2173; MultAcc: 0.6031; BinAcc: 0.7783; 
 	 	 ValLoss: 1.2236; ValMultAcc: 0.6190; ValBinAcc: 0.7980

TEST Loss: 1.2300; AccMult: 0.6165; AccBin: 0.7932

ROC AUC SCORE
	 good: 0.8391
	 celeb: 0.8743
	 casino: 0.8192
	 guns: 0.7006
	 illegal: 0.6605
	 narco: 0.7452
	 porno: 0.8470
	 yellow: 0.7148
Peak memory usage by Pytorch tensors: 2045.16 Mb
