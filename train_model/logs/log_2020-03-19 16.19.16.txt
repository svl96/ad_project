Logging Timestamp 2020-03-19 16.19.16

Namespace(batch=50, batch_count=1, checkpoint=-1, data_dir='data', epochs=8, memory_usage=True, model='resnet50', no_scheduler=True, num_workers=4, opt_lr=0.001, out='models/model_resnet50_2020-03-19 16.19.16.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model require grad = False
device cuda:0
Batch size 50
Classes: good celebrity casino guns illegal narcotics porno yellow
Run training

Step: 200; 68.79.s Loss: 1.2032; AccMul 0.6330 AccBin 0.7866
Step: 400; 124.99.s Loss: 1.1431; AccMul 0.6485 AccBin 0.7922
Step: 600; 177.50.s Loss: 1.1124; AccMul 0.6527 AccBin 0.7952
Step: 800; 230.06.s Loss: 1.0897; AccMul 0.6593 AccBin 0.7978
Step: 1000; 282.78.s Loss: 1.0669; AccMul 0.6659 AccBin 0.8011
Epoch [1/8] Time: 372.42s; BatchTime:0.28s; Loss: 1.0649; MultAcc: 0.6666; BinAcc: 0.8012; 
 	 	 ValLoss: 0.9805; ValMultAcc: 0.7106; ValBinAcc: 0.8307
Step: 200; 57.30.s Loss: 0.9627; AccMul 0.6872 AccBin 0.8106
Step: 400; 113.20.s Loss: 0.9574; AccMul 0.6930 AccBin 0.8146
Step: 600; 168.14.s Loss: 0.9629; AccMul 0.6903 AccBin 0.8141
Step: 800; 222.41.s Loss: 0.9625; AccMul 0.6910 AccBin 0.8145
Step: 1000; 276.31.s Loss: 0.9508; AccMul 0.6979 AccBin 0.8193
Epoch [2/8] Time: 357.77s; BatchTime:0.28s; Loss: 0.9496; MultAcc: 0.6987; BinAcc: 0.8196; 
 	 	 ValLoss: 0.9614; ValMultAcc: 0.6613; ValBinAcc: 0.7785
Step: 200; 55.70.s Loss: 0.8957; AccMul 0.7216 AccBin 0.8330
Step: 400; 110.83.s Loss: 0.8942; AccMul 0.7189 AccBin 0.8315
Step: 600; 165.54.s Loss: 0.8948; AccMul 0.7187 AccBin 0.8336
Step: 800; 219.87.s Loss: 0.8921; AccMul 0.7163 AccBin 0.8307
Step: 1000; 273.88.s Loss: 0.8879; AccMul 0.7173 AccBin 0.8315
Epoch [3/8] Time: 357.23s; BatchTime:0.27s; Loss: 0.8920; MultAcc: 0.7164; BinAcc: 0.8313; 
 	 	 ValLoss: 0.8862; ValMultAcc: 0.7072; ValBinAcc: 0.8135
Step: 200; 55.12.s Loss: 0.8610; AccMul 0.7299 AccBin 0.8394
Step: 400; 109.89.s Loss: 0.8558; AccMul 0.7277 AccBin 0.8362
Step: 600; 164.38.s Loss: 0.8637; AccMul 0.7260 AccBin 0.8355
Step: 800; 218.61.s Loss: 0.8598; AccMul 0.7241 AccBin 0.8336
Step: 1000; 272.65.s Loss: 0.8601; AccMul 0.7246 AccBin 0.8345
Epoch [4/8] Time: 357.36s; BatchTime:0.27s; Loss: 0.8630; MultAcc: 0.7247; BinAcc: 0.8344; 
 	 	 ValLoss: 0.8951; ValMultAcc: 0.6852; ValBinAcc: 0.7976
Step: 200; 54.81.s Loss: 0.7989; AccMul 0.7403 AccBin 0.8398
Step: 400; 109.38.s Loss: 0.8180; AccMul 0.7366 AccBin 0.8409
Step: 600; 163.73.s Loss: 0.8396; AccMul 0.7325 AccBin 0.8380
Step: 800; 217.88.s Loss: 0.8320; AccMul 0.7344 AccBin 0.8413
Step: 1000; 271.87.s Loss: 0.8291; AccMul 0.7346 AccBin 0.8412
Epoch [5/8] Time: 357.46s; BatchTime:0.27s; Loss: 0.8291; MultAcc: 0.7341; BinAcc: 0.8406; 
 	 	 ValLoss: 0.9076; ValMultAcc: 0.7613; ValBinAcc: 0.8595
Step: 200; 54.61.s Loss: 0.7851; AccMul 0.7378 AccBin 0.8381
Step: 400; 109.04.s Loss: 0.7834; AccMul 0.7418 AccBin 0.8422
Step: 600; 163.29.s Loss: 0.7901; AccMul 0.7405 AccBin 0.8422
Step: 800; 217.39.s Loss: 0.7930; AccMul 0.7418 AccBin 0.8435
Step: 1000; 271.36.s Loss: 0.7976; AccMul 0.7425 AccBin 0.8439
Epoch [6/8] Time: 357.45s; BatchTime:0.27s; Loss: 0.7980; MultAcc: 0.7429; BinAcc: 0.8439; 
 	 	 ValLoss: 0.8729; ValMultAcc: 0.7455; ValBinAcc: 0.8472
Step: 200; 54.47.s Loss: 0.7551; AccMul 0.7535 AccBin 0.8481
Step: 400; 108.79.s Loss: 0.7695; AccMul 0.7520 AccBin 0.8500
Step: 600; 162.99.s Loss: 0.7717; AccMul 0.7508 AccBin 0.8494
Step: 800; 217.07.s Loss: 0.7768; AccMul 0.7478 AccBin 0.8472
Step: 1000; 271.03.s Loss: 0.7741; AccMul 0.7483 AccBin 0.8472
Epoch [7/8] Time: 357.70s; BatchTime:0.27s; Loss: 0.7739; MultAcc: 0.7492; BinAcc: 0.8479; 
 	 	 ValLoss: 0.8555; ValMultAcc: 0.7509; ValBinAcc: 0.8504
Step: 200; 54.38.s Loss: 0.7247; AccMul 0.7608 AccBin 0.8510
Step: 400; 108.66.s Loss: 0.7461; AccMul 0.7566 AccBin 0.8501
Step: 600; 162.82.s Loss: 0.7559; AccMul 0.7569 AccBin 0.8506
Step: 800; 217.01.s Loss: 0.7531; AccMul 0.7563 AccBin 0.8499
Step: 1000; 271.05.s Loss: 0.7489; AccMul 0.7571 AccBin 0.8499
Epoch [8/8] Time: 360.83s; BatchTime:0.27s; Loss: 0.7485; MultAcc: 0.7567; BinAcc: 0.8497; 
 	 	 ValLoss: 0.8293; ValMultAcc: 0.7472; ValBinAcc: 0.8430

TEST Loss: 0.8766; AccMult: 0.7646; AccBin: 0.8661

ROC AUC SCORE
	 good: 0.9031
	 celebrity: 0.8884
	 casino: 0.9099
	 guns: 0.9450
	 illegal: 0.8714
	 narcotics: 0.8197
	 porno: 0.9187
	 yellow: 0.8060
Peak memory usage by Pytorch tensors: 676.33 Mb
