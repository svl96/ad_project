Logging Timestamp 2020-03-19 12.30.49

Namespace(batch=100, batch_count=20, checkpoint=-1, data_dir='data', epochs=2, memory_usage=True, model='resnet50', num_workers=4, opt_lr=0.001, out='models/model_resnet50_2020-03-19 12.30.49.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
device cuda:0
Batch size 100
Classes: good celebrity casino guns illegal porno yellow
Run training

Step: 100; 70.17.s Loss: 1.1601; AccMul 0.6596 AccBin 0.7077
Step: 200; 127.09.s Loss: 1.0108; AccMul 0.6905 AccBin 0.7590
Step: 300; 186.40.s Loss: 0.9382; AccMul 0.7072 AccBin 0.7821
Step: 400; 244.21.s Loss: 0.8919; AccMul 0.7185 AccBin 0.7953
Step: 500; 301.63.s Loss: 0.8597; AccMul 0.7261 AccBin 0.8018
Epoch [1/2] Time: 397.74s; BatchTime:0.60s; Loss: 0.8531; MultAcc: 0.7276; BinAcc: 0.8030; 
 	 	 ValLoss: 0.6811; ValMultAcc: 0.7789; ValBinAcc: 0.8482
Step: 100; 61.76.s Loss: 0.6956; AccMul 0.7697 AccBin 0.8377
Step: 200; 122.14.s Loss: 0.6895; AccMul 0.7686 AccBin 0.8366
Step: 300; 181.98.s Loss: 0.6893; AccMul 0.7681 AccBin 0.8369
Step: 400; 242.48.s Loss: 0.6877; AccMul 0.7684 AccBin 0.8374
Step: 500; 301.94.s Loss: 0.6837; AccMul 0.7700 AccBin 0.8391
Epoch [2/2] Time: 400.30s; BatchTime:0.60s; Loss: 0.6836; MultAcc: 0.7698; BinAcc: 0.8389; 
 	 	 ValLoss: 0.6487; ValMultAcc: 0.7864; ValBinAcc: 0.8517

TEST Loss: 0.6414; AccMult: 0.7850; AccBin: 0.8495

ROC AUC SCORE
	 good: 0.9195
	 celebrity: 0.9388
	 casino: 0.8610
	 guns: 0.8937
	 illegal: 0.7957
	 porno: 0.9112
	 yellow: 0.7809
Peak memory usage by Pytorch tensors: 1242.45 Mb
