Logging Timestamp 2020-04-18 16.56.20

Namespace(batch=100, batch_count=1, checkpoint=-1, data_dir='data_new', epochs=10, memory_usage=True, model='resnet50', no_scheduler=False, num_workers=4, opt_lr=0.01, out='models/model_resnet50_2020-04-18 16.56.20.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model require grad = False
device cuda:0
Adaptive
Combine loss alpha=0.9
Batch size 100
Classes: good celeb casino guns illegal narco porno yellow
Run training

Step: 100; 68.97.s Loss: 0.7698; AccMul 0.5302 AccBin 0.7709
Step: 200; 125.24.s Loss: 0.6850; AccMul 0.5645 AccBin 0.7788
Step: 300; 216.44.s Loss: 0.6586; AccMul 0.5776 AccBin 0.7828
Step: 400; 322.21.s Loss: 0.6451; AccMul 0.5865 AccBin 0.7877
Step: 500; 430.86.s Loss: 0.6376; AccMul 0.5910 AccBin 0.7892
Step: 600; 534.78.s Loss: 0.6359; AccMul 0.5925 AccBin 0.7886
Step: 700; 640.38.s Loss: 0.6361; AccMul 0.5949 AccBin 0.7897
Step: 800; 754.30.s Loss: 0.6320; AccMul 0.5965 AccBin 0.7914
Step: 900; 863.44.s Loss: 0.6285; AccMul 0.5989 AccBin 0.7924
Step: 1000; 968.15.s Loss: 0.6270; AccMul 0.5995 AccBin 0.7932
Epoch [1/10] Time: 1265.76s; BatchTime:0.97s; Loss: 0.6258; MultAcc: 0.6000; BinAcc: 0.7935; 
 	 	 ValLoss: 0.6355; ValMultAcc: 0.5816; ValBinAcc: 0.8130
Learning rate: [0.01]; Count: 5
Step: 100; 98.33.s Loss: 0.6988; AccMul 0.5892 AccBin 0.7779
Step: 200; 196.78.s Loss: 0.6341; AccMul 0.6150 AccBin 0.7891
Step: 300; 293.11.s Loss: 0.6065; AccMul 0.6272 AccBin 0.7963
Step: 400; 392.97.s Loss: 0.5926; AccMul 0.6339 AccBin 0.8009
Step: 500; 494.07.s Loss: 0.5829; AccMul 0.6402 AccBin 0.8046
Step: 600; 595.85.s Loss: 0.5766; AccMul 0.6449 AccBin 0.8057
Step: 700; 698.02.s Loss: 0.5716; AccMul 0.6476 AccBin 0.8075
Step: 800; 800.77.s Loss: 0.5681; AccMul 0.6495 AccBin 0.8085
Step: 900; 907.64.s Loss: 0.5647; AccMul 0.6520 AccBin 0.8099
Step: 1000; 1015.27.s Loss: 0.5623; AccMul 0.6537 AccBin 0.8109
Epoch [2/10] Time: 1369.00s; BatchTime:1.02s; Loss: 0.5613; MultAcc: 0.6547; BinAcc: 0.8112; 
 	 	 ValLoss: 0.5282; ValMultAcc: 0.6892; ValBinAcc: 0.8226
Learning rate: [0.0025]; Count: 10
Step: 100; 102.40.s Loss: 0.5485; AccMul 0.6664 AccBin 0.8136
Step: 200; 204.58.s Loss: 0.5388; AccMul 0.6743 AccBin 0.8200
Step: 300; 305.39.s Loss: 0.5363; AccMul 0.6774 AccBin 0.8212
Step: 400; 406.86.s Loss: 0.5314; AccMul 0.6821 AccBin 0.8246
Step: 500; 509.31.s Loss: 0.5312; AccMul 0.6819 AccBin 0.8242
Step: 600; 612.46.s Loss: 0.5297; AccMul 0.6850 AccBin 0.8256
Step: 700; 714.53.s Loss: 0.5289; AccMul 0.6863 AccBin 0.8262
Step: 800; 818.26.s Loss: 0.5285; AccMul 0.6867 AccBin 0.8263
Step: 900; 922.11.s Loss: 0.5279; AccMul 0.6872 AccBin 0.8268
Step: 1000; 1026.14.s Loss: 0.5276; AccMul 0.6876 AccBin 0.8267
Epoch [3/10] Time: 1340.80s; BatchTime:1.03s; Loss: 0.5273; MultAcc: 0.6878; BinAcc: 0.8269; 
 	 	 ValLoss: 0.5158; ValMultAcc: 0.7072; ValBinAcc: 0.8319
Learning rate: [0.00125]; Count: 15
Step: 100; 103.13.s Loss: 0.5488; AccMul 0.6664 AccBin 0.8138
Step: 200; 206.02.s Loss: 0.5405; AccMul 0.6761 AccBin 0.8179
Step: 300; 308.94.s Loss: 0.5353; AccMul 0.6800 AccBin 0.8210
Step: 400; 413.05.s Loss: 0.5334; AccMul 0.6823 AccBin 0.8219
Step: 500; 518.56.s Loss: 0.5315; AccMul 0.6850 AccBin 0.8234
Step: 600; 623.51.s Loss: 0.5297; AccMul 0.6879 AccBin 0.8248
Step: 700; 730.09.s Loss: 0.5275; AccMul 0.6897 AccBin 0.8263
Step: 800; 837.83.s Loss: 0.5263; AccMul 0.6911 AccBin 0.8272
Step: 900; 946.59.s Loss: 0.5249; AccMul 0.6925 AccBin 0.8281
Step: 1000; 1055.86.s Loss: 0.5241; AccMul 0.6937 AccBin 0.8288
Epoch [4/10] Time: 1475.02s; BatchTime:1.06s; Loss: 0.5233; MultAcc: 0.6946; BinAcc: 0.8294; 
 	 	 ValLoss: 0.5142; ValMultAcc: 0.7138; ValBinAcc: 0.8286
Learning rate: [0.000625]; Count: 20
Step: 100; 106.32.s Loss: 0.5231; AccMul 0.6953 AccBin 0.8291
Step: 200; 212.72.s Loss: 0.5208; AccMul 0.6998 AccBin 0.8314
Step: 300; 320.17.s Loss: 0.5204; AccMul 0.7005 AccBin 0.8304
Step: 400; 428.44.s Loss: 0.5182; AccMul 0.7031 AccBin 0.8324
Step: 500; 537.43.s Loss: 0.5153; AccMul 0.7056 AccBin 0.8346
Step: 600; 647.36.s Loss: 0.5138; AccMul 0.7069 AccBin 0.8359
Step: 700; 757.25.s Loss: 0.5127; AccMul 0.7087 AccBin 0.8368
Step: 800; 866.59.s Loss: 0.5111; AccMul 0.7106 AccBin 0.8383
Step: 900; 975.98.s Loss: 0.5099; AccMul 0.7120 AccBin 0.8390
Step: 1000; 1085.71.s Loss: 0.5093; AccMul 0.7125 AccBin 0.8397
Epoch [5/10] Time: 1503.95s; BatchTime:1.09s; Loss: 0.5088; MultAcc: 0.7130; BinAcc: 0.8402; 
 	 	 ValLoss: 0.5024; ValMultAcc: 0.7247; ValBinAcc: 0.8451
Learning rate: [0.0003125]; Count: 25
Step: 100; 108.94.s Loss: 0.4918; AccMul 0.7338 AccBin 0.8551
Step: 200; 217.62.s Loss: 0.4948; AccMul 0.7329 AccBin 0.8509
Step: 300; 326.61.s Loss: 0.4952; AccMul 0.7294 AccBin 0.8509
Step: 400; 435.72.s Loss: 0.4949; AccMul 0.7302 AccBin 0.8512
Step: 500; 544.92.s Loss: 0.4935; AccMul 0.7312 AccBin 0.8521
Step: 600; 654.23.s Loss: 0.4940; AccMul 0.7300 AccBin 0.8514
Step: 700; 763.61.s Loss: 0.4937; AccMul 0.7306 AccBin 0.8515
Step: 800; 873.94.s Loss: 0.4927; AccMul 0.7316 AccBin 0.8528
Step: 900; 984.84.s Loss: 0.4925; AccMul 0.7318 AccBin 0.8525
Step: 1000; 1095.97.s Loss: 0.4924; AccMul 0.7321 AccBin 0.8526
Epoch [6/10] Time: 1450.40s; BatchTime:1.10s; Loss: 0.4922; MultAcc: 0.7324; BinAcc: 0.8529; 
 	 	 ValLoss: 0.4969; ValMultAcc: 0.7351; ValBinAcc: 0.8471
Learning rate: [0.00015625]; Count: 30
Step: 100; 110.00.s Loss: 0.4733; AccMul 0.7603 AccBin 0.8696
Step: 200; 220.00.s Loss: 0.4741; AccMul 0.7567 AccBin 0.8694
Step: 300; 330.82.s Loss: 0.4771; AccMul 0.7524 AccBin 0.8667
Step: 400; 442.25.s Loss: 0.4790; AccMul 0.7510 AccBin 0.8646
Step: 500; 554.10.s Loss: 0.4799; AccMul 0.7498 AccBin 0.8639
Step: 600; 666.37.s Loss: 0.4805; AccMul 0.7490 AccBin 0.8636
Step: 700; 779.14.s Loss: 0.4802; AccMul 0.7496 AccBin 0.8639
Step: 800; 892.24.s Loss: 0.4805; AccMul 0.7491 AccBin 0.8637
Step: 900; 1005.74.s Loss: 0.4806; AccMul 0.7487 AccBin 0.8637
Step: 1000; 1119.62.s Loss: 0.4805; AccMul 0.7486 AccBin 0.8634
Epoch [7/10] Time: 1577.04s; BatchTime:1.12s; Loss: 0.4803; MultAcc: 0.7487; BinAcc: 0.8637; 
 	 	 ValLoss: 0.4965; ValMultAcc: 0.7363; ValBinAcc: 0.8474
Learning rate: [7.8125e-05]; Count: 35
Step: 100; 112.42.s Loss: 0.4640; AccMul 0.7683 AccBin 0.8779
Step: 200; 224.97.s Loss: 0.4641; AccMul 0.7685 AccBin 0.8772
Step: 300; 338.28.s Loss: 0.4626; AccMul 0.7703 AccBin 0.8784
Step: 400; 452.04.s Loss: 0.4615; AccMul 0.7704 AccBin 0.8789
Step: 500; 566.18.s Loss: 0.4609; AccMul 0.7716 AccBin 0.8791
Step: 600; 680.98.s Loss: 0.4604; AccMul 0.7714 AccBin 0.8796
Step: 700; 796.14.s Loss: 0.4599; AccMul 0.7713 AccBin 0.8797
Step: 800; 912.02.s Loss: 0.4602; AccMul 0.7714 AccBin 0.8793
Step: 900; 1029.33.s Loss: 0.4599; AccMul 0.7713 AccBin 0.8797
Step: 1000; 1147.10.s Loss: 0.4599; AccMul 0.7714 AccBin 0.8797
Epoch [8/10] Time: 1659.75s; BatchTime:1.15s; Loss: 0.4603; MultAcc: 0.7711; BinAcc: 0.8794; 
 	 	 ValLoss: 0.4936; ValMultAcc: 0.7426; ValBinAcc: 0.8520
Learning rate: [3.90625e-05]; Count: 40
Step: 100; 115.17.s Loss: 0.4432; AccMul 0.7940 AccBin 0.8929
Step: 200; 230.50.s Loss: 0.4402; AccMul 0.7966 AccBin 0.8972
Step: 300; 346.32.s Loss: 0.4414; AccMul 0.7972 AccBin 0.8971
Step: 400; 462.72.s Loss: 0.4399; AccMul 0.7958 AccBin 0.8986
Step: 500; 579.67.s Loss: 0.4395; AccMul 0.7966 AccBin 0.8987
Step: 600; 697.09.s Loss: 0.4400; AccMul 0.7957 AccBin 0.8977
Step: 700; 814.98.s Loss: 0.4401; AccMul 0.7954 AccBin 0.8980
Step: 800; 933.15.s Loss: 0.4405; AccMul 0.7950 AccBin 0.8977
Step: 900; 1051.92.s Loss: 0.4401; AccMul 0.7957 AccBin 0.8980
Step: 1000; 1171.08.s Loss: 0.4399; AccMul 0.7956 AccBin 0.8980
Epoch [9/10] Time: 1668.07s; BatchTime:1.17s; Loss: 0.4400; MultAcc: 0.7951; BinAcc: 0.8977; 
 	 	 ValLoss: 0.4943; ValMultAcc: 0.7451; ValBinAcc: 0.8552
Learning rate: [1.953125e-05]; Count: 45
Step: 100; 117.45.s Loss: 0.4255; AccMul 0.8116 AccBin 0.9123
Step: 200; 234.93.s Loss: 0.4255; AccMul 0.8104 AccBin 0.9113
Step: 300; 352.80.s Loss: 0.4252; AccMul 0.8110 AccBin 0.9118
Step: 400; 471.01.s Loss: 0.4256; AccMul 0.8105 AccBin 0.9111
Step: 500; 589.44.s Loss: 0.4246; AccMul 0.8114 AccBin 0.9122
Step: 600; 708.25.s Loss: 0.4248; AccMul 0.8099 AccBin 0.9120
Step: 700; 827.39.s Loss: 0.4253; AccMul 0.8099 AccBin 0.9111
Step: 800; 947.22.s Loss: 0.4247; AccMul 0.8104 AccBin 0.9115
Step: 900; 1067.46.s Loss: 0.4252; AccMul 0.8102 AccBin 0.9110
Step: 1000; 1187.55.s Loss: 0.4253; AccMul 0.8097 AccBin 0.9109
Epoch [10/10] Time: 1649.03s; BatchTime:1.19s; Loss: 0.4258; MultAcc: 0.8091; BinAcc: 0.9108; 
 	 	 ValLoss: 0.4963; ValMultAcc: 0.7468; ValBinAcc: 0.8532
Learning rate: [9.765625e-06]; Count: 50

TEST Loss: 0.4931; AccMult: 0.7400; AccBin: 0.8547

