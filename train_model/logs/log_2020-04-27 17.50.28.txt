Logging Timestamp 2020-04-27 17.50.28

Namespace(batch=100, batch_count=1, checkpoint=-1, data_dir='data_new', epochs=10, memory_usage=True, model='resnet50', no_scheduler=False, num_workers=4, opt_lr=0.01, out='models/model_resnet50_2020-04-27 17.50.28.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model require grad = False
device cuda:0
Adaptive
Weights [1, 1, 1, 1, 1, 1, 1, 2]
Batch size 100
Classes: good celeb casino guns illegal narco porno yellow
Run training

Step: 100; 68.86.s Loss: 1.7842; AccMul 0.5409 AccBin 0.7496
Step: 200; 122.88.s Loss: 1.4316; AccMul 0.5726 AccBin 0.7656
Step: 300; 177.22.s Loss: 1.3568; AccMul 0.5766 AccBin 0.7645
Step: 400; 233.50.s Loss: 1.3256; AccMul 0.5792 AccBin 0.7664
Step: 500; 292.31.s Loss: 1.3052; AccMul 0.5822 AccBin 0.7678
Step: 600; 349.36.s Loss: 1.2859; AccMul 0.5848 AccBin 0.7692
Step: 700; 408.97.s Loss: 1.2782; AccMul 0.5849 AccBin 0.7687
Step: 800; 469.12.s Loss: 1.2632; AccMul 0.5877 AccBin 0.7711
Step: 900; 529.14.s Loss: 1.2747; AccMul 0.5867 AccBin 0.7706
Step: 1000; 588.41.s Loss: 1.2664; AccMul 0.5883 AccBin 0.7721
Epoch [1/10] Time: 782.06s; BatchTime:0.59s; Loss: 1.2600; MultAcc: 0.5892; BinAcc: 0.7727; 
 	 	 ValLoss: 1.2703; ValMultAcc: 0.5966; ValBinAcc: 0.7904
Learning rate: [0.01]; Count: 5
Step: 100; 59.96.s Loss: 1.4924; AccMul 0.5866 AccBin 0.7700
Step: 200; 120.01.s Loss: 1.2430; AccMul 0.6048 AccBin 0.7837
Step: 300; 180.11.s Loss: 1.1469; AccMul 0.6155 AccBin 0.7907
Step: 400; 240.28.s Loss: 1.0886; AccMul 0.6239 AccBin 0.7958
Step: 500; 300.60.s Loss: 1.0615; AccMul 0.6243 AccBin 0.7950
Step: 600; 360.79.s Loss: 1.0377; AccMul 0.6272 AccBin 0.7969
Step: 700; 421.00.s Loss: 1.0204; AccMul 0.6301 AccBin 0.7979
Step: 800; 481.29.s Loss: 1.0052; AccMul 0.6332 AccBin 0.7992
Step: 900; 541.62.s Loss: 0.9906; AccMul 0.6360 AccBin 0.8012
Step: 1000; 601.87.s Loss: 0.9783; AccMul 0.6384 AccBin 0.8027
Epoch [2/10] Time: 811.16s; BatchTime:0.60s; Loss: 0.9724; MultAcc: 0.6394; BinAcc: 0.8032; 
 	 	 ValLoss: 0.9004; ValMultAcc: 0.6923; ValBinAcc: 0.8115
Learning rate: [0.0025]; Count: 10
Step: 100; 60.78.s Loss: 0.8884; AccMul 0.6508 AccBin 0.8171
Step: 200; 121.69.s Loss: 0.8732; AccMul 0.6547 AccBin 0.8179
Step: 300; 182.75.s Loss: 0.8625; AccMul 0.6609 AccBin 0.8189
Step: 400; 243.02.s Loss: 0.8547; AccMul 0.6651 AccBin 0.8199
Step: 500; 303.01.s Loss: 0.8529; AccMul 0.6662 AccBin 0.8211
Step: 600; 362.80.s Loss: 0.8491; AccMul 0.6681 AccBin 0.8215
Step: 700; 422.74.s Loss: 0.8453; AccMul 0.6696 AccBin 0.8213
Step: 800; 482.41.s Loss: 0.8421; AccMul 0.6704 AccBin 0.8224
Step: 900; 542.06.s Loss: 0.8401; AccMul 0.6705 AccBin 0.8223
Step: 1000; 601.40.s Loss: 0.8376; AccMul 0.6714 AccBin 0.8223
Epoch [3/10] Time: 784.13s; BatchTime:0.60s; Loss: 0.8360; MultAcc: 0.6722; BinAcc: 0.8227; 
 	 	 ValLoss: 0.7976; ValMultAcc: 0.7007; ValBinAcc: 0.8363
Learning rate: [0.00125]; Count: 15
Step: 100; 60.45.s Loss: 0.9037; AccMul 0.6404 AccBin 0.8020
Step: 200; 120.85.s Loss: 0.8795; AccMul 0.6551 AccBin 0.8125
Step: 300; 181.01.s Loss: 0.8664; AccMul 0.6589 AccBin 0.8141
Step: 400; 241.03.s Loss: 0.8548; AccMul 0.6640 AccBin 0.8174
Step: 500; 301.18.s Loss: 0.8435; AccMul 0.6686 AccBin 0.8194
Step: 600; 360.89.s Loss: 0.8369; AccMul 0.6715 AccBin 0.8205
Step: 700; 421.16.s Loss: 0.8306; AccMul 0.6751 AccBin 0.8232
Step: 800; 481.36.s Loss: 0.8257; AccMul 0.6772 AccBin 0.8245
Step: 900; 541.71.s Loss: 0.8234; AccMul 0.6783 AccBin 0.8250
Step: 1000; 601.73.s Loss: 0.8209; AccMul 0.6796 AccBin 0.8257
Epoch [4/10] Time: 782.56s; BatchTime:0.60s; Loss: 0.8179; MultAcc: 0.6810; BinAcc: 0.8266; 
 	 	 ValLoss: 0.7624; ValMultAcc: 0.7146; ValBinAcc: 0.8392
Learning rate: [0.000625]; Count: 20
Step: 100; 60.49.s Loss: 0.8263; AccMul 0.6830 AccBin 0.8308
Step: 200; 121.16.s Loss: 0.7991; AccMul 0.6922 AccBin 0.8330
Step: 300; 181.82.s Loss: 0.7849; AccMul 0.6959 AccBin 0.8340
Step: 400; 242.19.s Loss: 0.7782; AccMul 0.6974 AccBin 0.8340
Step: 500; 302.39.s Loss: 0.7758; AccMul 0.6978 AccBin 0.8351
Step: 600; 362.45.s Loss: 0.7711; AccMul 0.7001 AccBin 0.8369
Step: 700; 422.43.s Loss: 0.7684; AccMul 0.7013 AccBin 0.8367
Step: 800; 482.25.s Loss: 0.7625; AccMul 0.7034 AccBin 0.8380
Step: 900; 541.99.s Loss: 0.7594; AccMul 0.7045 AccBin 0.8382
Step: 1000; 601.62.s Loss: 0.7584; AccMul 0.7049 AccBin 0.8384
Epoch [5/10] Time: 773.22s; BatchTime:0.60s; Loss: 0.7588; MultAcc: 0.7049; BinAcc: 0.8381; 
 	 	 ValLoss: 0.7373; ValMultAcc: 0.7253; ValBinAcc: 0.8447
Learning rate: [0.0003125]; Count: 25
Step: 100; 60.33.s Loss: 0.6940; AccMul 0.7320 AccBin 0.8504
Step: 200; 120.63.s Loss: 0.7045; AccMul 0.7257 AccBin 0.8466
Step: 300; 180.91.s Loss: 0.7060; AccMul 0.7247 AccBin 0.8469
Step: 400; 241.16.s Loss: 0.7051; AccMul 0.7248 AccBin 0.8465
Step: 500; 301.40.s Loss: 0.7025; AccMul 0.7261 AccBin 0.8476
Step: 600; 362.11.s Loss: 0.7026; AccMul 0.7261 AccBin 0.8471
Step: 700; 422.80.s Loss: 0.7007; AccMul 0.7272 AccBin 0.8478
Step: 800; 483.10.s Loss: 0.6995; AccMul 0.7281 AccBin 0.8487
Step: 900; 543.37.s Loss: 0.6986; AccMul 0.7294 AccBin 0.8494
Step: 1000; 603.61.s Loss: 0.6979; AccMul 0.7294 AccBin 0.8491
Epoch [6/10] Time: 788.85s; BatchTime:0.60s; Loss: 0.6990; MultAcc: 0.7289; BinAcc: 0.8490; 
 	 	 ValLoss: 0.7133; ValMultAcc: 0.7343; ValBinAcc: 0.8514
Learning rate: [0.00015625]; Count: 30
Step: 100; 60.59.s Loss: 0.6422; AccMul 0.7513 AccBin 0.8600
Step: 200; 121.25.s Loss: 0.6494; AccMul 0.7477 AccBin 0.8558
Step: 300; 182.00.s Loss: 0.6503; AccMul 0.7491 AccBin 0.8572
Step: 400; 242.83.s Loss: 0.6502; AccMul 0.7484 AccBin 0.8577
Step: 500; 303.72.s Loss: 0.6513; AccMul 0.7483 AccBin 0.8580
Step: 600; 364.68.s Loss: 0.6528; AccMul 0.7463 AccBin 0.8567
Step: 700; 425.71.s Loss: 0.6529; AccMul 0.7467 AccBin 0.8562
Step: 800; 487.26.s Loss: 0.6550; AccMul 0.7462 AccBin 0.8560
Step: 900; 549.14.s Loss: 0.6521; AccMul 0.7476 AccBin 0.8574
Step: 1000; 611.15.s Loss: 0.6524; AccMul 0.7475 AccBin 0.8571
Epoch [7/10] Time: 838.23s; BatchTime:0.61s; Loss: 0.6528; MultAcc: 0.7473; BinAcc: 0.8571; 
 	 	 ValLoss: 0.7327; ValMultAcc: 0.7377; ValBinAcc: 0.8483
Learning rate: [7.8125e-05]; Count: 35
Step: 100; 61.36.s Loss: 0.5802; AccMul 0.7740 AccBin 0.8702
Step: 200; 122.88.s Loss: 0.5737; AccMul 0.7789 AccBin 0.8734
Step: 300; 184.54.s Loss: 0.5702; AccMul 0.7794 AccBin 0.8730
Step: 400; 246.37.s Loss: 0.5711; AccMul 0.7790 AccBin 0.8712
Step: 500; 308.31.s Loss: 0.5748; AccMul 0.7778 AccBin 0.8717
Step: 600; 370.43.s Loss: 0.5746; AccMul 0.7779 AccBin 0.8717
Step: 700; 432.75.s Loss: 0.5757; AccMul 0.7767 AccBin 0.8713
Step: 800; 495.49.s Loss: 0.5746; AccMul 0.7773 AccBin 0.8720
Step: 900; 558.58.s Loss: 0.5743; AccMul 0.7774 AccBin 0.8717
Step: 1000; 621.63.s Loss: 0.5734; AccMul 0.7777 AccBin 0.8718
Epoch [8/10] Time: 889.65s; BatchTime:0.62s; Loss: 0.5744; MultAcc: 0.7777; BinAcc: 0.8718; 
 	 	 ValLoss: 0.7287; ValMultAcc: 0.7374; ValBinAcc: 0.8486
Learning rate: [3.90625e-05]; Count: 40
Step: 100; 62.49.s Loss: 0.4955; AccMul 0.8076 AccBin 0.8896
Step: 200; 125.29.s Loss: 0.4987; AccMul 0.8076 AccBin 0.8871
Step: 300; 188.42.s Loss: 0.5010; AccMul 0.8075 AccBin 0.8872
Step: 400; 251.79.s Loss: 0.5010; AccMul 0.8075 AccBin 0.8871
Step: 500; 315.33.s Loss: 0.5001; AccMul 0.8079 AccBin 0.8876
Step: 600; 379.08.s Loss: 0.4978; AccMul 0.8084 AccBin 0.8875
Step: 700; 442.83.s Loss: 0.4963; AccMul 0.8091 AccBin 0.8880
Step: 800; 506.64.s Loss: 0.4979; AccMul 0.8087 AccBin 0.8877
Step: 900; 570.55.s Loss: 0.4989; AccMul 0.8082 AccBin 0.8871
Step: 1000; 634.54.s Loss: 0.4998; AccMul 0.8077 AccBin 0.8865
Epoch [9/10] Time: 919.62s; BatchTime:0.63s; Loss: 0.5008; MultAcc: 0.8074; BinAcc: 0.8863; 
 	 	 ValLoss: 0.7561; ValMultAcc: 0.7403; ValBinAcc: 0.8458
Learning rate: [1.953125e-05]; Count: 45
Step: 100; 63.72.s Loss: 0.4461; AccMul 0.8266 AccBin 0.8983
Step: 200; 127.59.s Loss: 0.4470; AccMul 0.8278 AccBin 0.8978
Step: 300; 191.62.s Loss: 0.4474; AccMul 0.8271 AccBin 0.8965
Step: 400; 256.14.s Loss: 0.4459; AccMul 0.8276 AccBin 0.8960
Step: 500; 320.94.s Loss: 0.4475; AccMul 0.8281 AccBin 0.8966
Step: 600; 385.93.s Loss: 0.4479; AccMul 0.8280 AccBin 0.8967
Step: 700; 451.27.s Loss: 0.4479; AccMul 0.8278 AccBin 0.8967
Step: 800; 516.95.s Loss: 0.4473; AccMul 0.8285 AccBin 0.8973
Step: 900; 582.68.s Loss: 0.4476; AccMul 0.8280 AccBin 0.8972
Step: 1000; 648.34.s Loss: 0.4463; AccMul 0.8290 AccBin 0.8978
Epoch [10/10] Time: 957.22s; BatchTime:0.65s; Loss: 0.4466; MultAcc: 0.8291; BinAcc: 0.8980; 
 	 	 ValLoss: 0.7884; ValMultAcc: 0.7453; ValBinAcc: 0.8482
Learning rate: [9.765625e-06]; Count: 50

TEST Loss: 0.7187; AccMult: 0.7305; AccBin: 0.8493

ROC AUC SCORE
	 good: 0.8968
	 celeb: 0.8502
	 casino: 0.8230
	 guns: 0.8896
	 illegal: 0.8375
	 narco: 0.7997
	 porno: 0.8935
	 yellow: 0.8562
Peak memory usage by Pytorch tensors: 2312.71 Mb
