Logging Timestamp 2020-05-04 00.55.21

Namespace(adaptive=False, batch=100, batch_count=1, checkpoint=-1, data_dir='data_new', epochs=5, memory_usage=True, model='dense121', no_scheduler=False, num_workers=4, opt_lr=0.001, out='models/model_dense121_2020-05-04 00.55.21.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model dense121 require grad = False
device cuda:0
Adaptive
Weights [0.5, 1, 1, 1, 1, 1, 1, 0.5]
Batch size 100
Classes: good celeb casino guns illegal narco porno yellow
Run training

Step: 100; 65.13.s Loss: 1.3091; AccMul 0.5772 AccBin 0.7632
Step: 200; 118.90.s Loss: 1.1949; AccMul 0.6122 AccBin 0.7819
Step: 300; 172.74.s Loss: 1.1287; AccMul 0.6307 AccBin 0.7942
Step: 400; 226.55.s Loss: 1.0907; AccMul 0.6417 AccBin 0.8027
Step: 500; 281.12.s Loss: 1.0669; AccMul 0.6482 AccBin 0.8055
Step: 600; 335.69.s Loss: 1.0496; AccMul 0.6531 AccBin 0.8078
Step: 700; 389.37.s Loss: 1.0361; AccMul 0.6568 AccBin 0.8098
Step: 800; 441.68.s Loss: 1.0241; AccMul 0.6598 AccBin 0.8113
Step: 900; 493.93.s Loss: 1.0135; AccMul 0.6627 AccBin 0.8125
Step: 1000; 546.21.s Loss: 1.0046; AccMul 0.6656 AccBin 0.8137
Epoch [1/5] Time: 711.56s; BatchTime:0.54s; Loss: 1.0009; MultAcc: 0.6666; BinAcc: 0.8144; 
 	 	 ValLoss: 0.9149; ValMultAcc: 0.6836; ValBinAcc: 0.8296
Step: 100; 55.12.s Loss: 0.8788; AccMul 0.7005 AccBin 0.8325
Step: 200; 109.78.s Loss: 0.8909; AccMul 0.6966 AccBin 0.8285
Step: 300; 164.08.s Loss: 0.8867; AccMul 0.6972 AccBin 0.8306
Step: 400; 218.09.s Loss: 0.8945; AccMul 0.6951 AccBin 0.8300
Step: 500; 271.86.s Loss: 0.8946; AccMul 0.6964 AccBin 0.8306
Step: 600; 325.44.s Loss: 0.8951; AccMul 0.6970 AccBin 0.8312
Step: 700; 379.23.s Loss: 0.8947; AccMul 0.6978 AccBin 0.8319
Step: 800; 432.56.s Loss: 0.8924; AccMul 0.6991 AccBin 0.8320
Step: 900; 485.76.s Loss: 0.8903; AccMul 0.6996 AccBin 0.8320
Step: 1000; 539.77.s Loss: 0.8894; AccMul 0.7001 AccBin 0.8320
Epoch [2/5] Time: 715.92s; BatchTime:0.54s; Loss: 0.8894; MultAcc: 0.7002; BinAcc: 0.8319; 
 	 	 ValLoss: 0.8641; ValMultAcc: 0.7037; ValBinAcc: 0.8339
Step: 100; 54.66.s Loss: 0.8531; AccMul 0.7119 AccBin 0.8416
Step: 200; 109.51.s Loss: 0.8537; AccMul 0.7102 AccBin 0.8397
Step: 300; 164.35.s Loss: 0.8536; AccMul 0.7094 AccBin 0.8388
Step: 400; 219.58.s Loss: 0.8542; AccMul 0.7112 AccBin 0.8400
Step: 500; 274.96.s Loss: 0.8538; AccMul 0.7098 AccBin 0.8383
Step: 600; 330.55.s Loss: 0.8541; AccMul 0.7100 AccBin 0.8375
Step: 700; 385.01.s Loss: 0.8524; AccMul 0.7109 AccBin 0.8376
Step: 800; 439.26.s Loss: 0.8498; AccMul 0.7117 AccBin 0.8376
Step: 900; 493.39.s Loss: 0.8481; AccMul 0.7120 AccBin 0.8373
Step: 1000; 547.38.s Loss: 0.8489; AccMul 0.7112 AccBin 0.8369
Epoch [3/5] Time: 727.30s; BatchTime:0.55s; Loss: 0.8479; MultAcc: 0.7112; BinAcc: 0.8367; 
 	 	 ValLoss: 0.8603; ValMultAcc: 0.7190; ValBinAcc: 0.8375
Step: 100; 54.90.s Loss: 0.8055; AccMul 0.7220 AccBin 0.8409
Step: 200; 109.65.s Loss: 0.8038; AccMul 0.7207 AccBin 0.8425
Step: 300; 164.24.s Loss: 0.8071; AccMul 0.7205 AccBin 0.8421
Step: 400; 218.68.s Loss: 0.8173; AccMul 0.7184 AccBin 0.8404
Step: 500; 273.01.s Loss: 0.8217; AccMul 0.7178 AccBin 0.8415
Step: 600; 327.22.s Loss: 0.8212; AccMul 0.7173 AccBin 0.8405
Step: 700; 381.33.s Loss: 0.8236; AccMul 0.7174 AccBin 0.8404
Step: 800; 435.34.s Loss: 0.8234; AccMul 0.7184 AccBin 0.8412
Step: 900; 489.25.s Loss: 0.8245; AccMul 0.7183 AccBin 0.8409
Step: 1000; 543.06.s Loss: 0.8253; AccMul 0.7183 AccBin 0.8407
Epoch [4/5] Time: 695.87s; BatchTime:0.54s; Loss: 0.8251; MultAcc: 0.7187; BinAcc: 0.8410; 
 	 	 ValLoss: 0.8603; ValMultAcc: 0.7049; ValBinAcc: 0.8407
