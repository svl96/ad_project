Logging Timestamp 2020-05-06 11.50.06

Namespace(activate_random=True, adaptive=False, batch=100, batch_count=1, checkpoint=-1, data_dir='data_new', epochs=12, memory_usage=True, model='resnet50', no_scheduler=False, num_workers=4, opt_lr=0.0001, out='models/model_resnet50_2020-05-06 11.50.06.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model resnet50 require grad = False
device cuda:0
Adaptive
Weights [0.5, 1, 1, 1, 1, 1, 1, 0.5]
Optim Adam
LR: 0.0001 0.5
Batch size 100
Classes: good celeb casino guns illegal narco porno yellow
Run training

Count 10 Random 10 
Step: 100; 70.83.s Loss: 1.3318; AccMul 0.5670 AccBin 0.7505
Step: 200; 130.88.s Loss: 1.2108; AccMul 0.6032 AccBin 0.7742
Step: 300; 194.56.s Loss: 1.1502; AccMul 0.6206 AccBin 0.7870
Step: 400; 258.25.s Loss: 1.1150; AccMul 0.6288 AccBin 0.7924
Step: 500; 320.14.s Loss: 1.0877; AccMul 0.6373 AccBin 0.7968
Step: 600; 379.76.s Loss: 1.0709; AccMul 0.6420 AccBin 0.7999
Step: 700; 440.60.s Loss: 1.0559; AccMul 0.6462 AccBin 0.8016
Step: 800; 501.56.s Loss: 1.0450; AccMul 0.6493 AccBin 0.8043
Step: 900; 562.48.s Loss: 1.0367; AccMul 0.6528 AccBin 0.8065
Step: 1000; 624.98.s Loss: 1.0279; AccMul 0.6551 AccBin 0.8083
Epoch [1/12] Time: 818.01s; BatchTime:0.62s; Loss: 1.0227; MultAcc: 0.6566; BinAcc: 0.8093; 
 	 	 ValLoss: 0.9254; ValMultAcc: 0.6719; ValBinAcc: 0.8263
Step: 100; 62.95.s Loss: 0.9110; AccMul 0.6910 AccBin 0.8285
Step: 200; 125.14.s Loss: 0.9139; AccMul 0.6896 AccBin 0.8287
Step: 300; 186.01.s Loss: 0.9154; AccMul 0.6883 AccBin 0.8274
Step: 400; 246.00.s Loss: 0.9184; AccMul 0.6883 AccBin 0.8274
Step: 500; 305.99.s Loss: 0.9177; AccMul 0.6888 AccBin 0.8275
Step: 600; 365.07.s Loss: 0.9149; AccMul 0.6889 AccBin 0.8273
Step: 700; 424.62.s Loss: 0.9111; AccMul 0.6895 AccBin 0.8272
Step: 800; 485.23.s Loss: 0.9097; AccMul 0.6903 AccBin 0.8272
Step: 900; 545.70.s Loss: 0.9091; AccMul 0.6897 AccBin 0.8268
Step: 1000; 604.07.s Loss: 0.9084; AccMul 0.6902 AccBin 0.8271
Epoch [2/12] Time: 767.80s; BatchTime:0.60s; Loss: 0.9092; MultAcc: 0.6906; BinAcc: 0.8274; 
 	 	 ValLoss: 0.8824; ValMultAcc: 0.7013; ValBinAcc: 0.8372
Step: 100; 60.61.s Loss: 0.8711; AccMul 0.7050 AccBin 0.8390
Step: 200; 120.97.s Loss: 0.8697; AccMul 0.7055 AccBin 0.8369
Step: 300; 181.24.s Loss: 0.8670; AccMul 0.7053 AccBin 0.8347
Step: 400; 241.51.s Loss: 0.8669; AccMul 0.7047 AccBin 0.8343
Step: 500; 300.98.s Loss: 0.8646; AccMul 0.7058 AccBin 0.8347
Step: 600; 360.18.s Loss: 0.8677; AccMul 0.7054 AccBin 0.8350
Step: 700; 419.12.s Loss: 0.8690; AccMul 0.7051 AccBin 0.8346
Step: 800; 477.96.s Loss: 0.8683; AccMul 0.7054 AccBin 0.8351
Step: 900; 536.34.s Loss: 0.8690; AccMul 0.7044 AccBin 0.8343
Step: 1000; 594.68.s Loss: 0.8702; AccMul 0.7040 AccBin 0.8342
Epoch [3/12] Time: 753.95s; BatchTime:0.59s; Loss: 0.8696; MultAcc: 0.7044; BinAcc: 0.8343; 
 	 	 ValLoss: 0.8892; ValMultAcc: 0.6967; ValBinAcc: 0.8222
Step: 100; 59.61.s Loss: 0.8411; AccMul 0.7115 AccBin 0.8389
Step: 200; 118.97.s Loss: 0.8411; AccMul 0.7105 AccBin 0.8373
Step: 300; 178.07.s Loss: 0.8450; AccMul 0.7081 AccBin 0.8367
Step: 400; 236.78.s Loss: 0.8412; AccMul 0.7091 AccBin 0.8364
Step: 500; 295.23.s Loss: 0.8424; AccMul 0.7101 AccBin 0.8369
Step: 600; 353.37.s Loss: 0.8438; AccMul 0.7100 AccBin 0.8360
Step: 700; 411.26.s Loss: 0.8434; AccMul 0.7100 AccBin 0.8369
Step: 800; 468.92.s Loss: 0.8431; AccMul 0.7106 AccBin 0.8372
Step: 900; 526.42.s Loss: 0.8437; AccMul 0.7104 AccBin 0.8370
Step: 1000; 583.70.s Loss: 0.8435; AccMul 0.7108 AccBin 0.8374
Epoch [4/12] Time: 719.84s; BatchTime:0.58s; Loss: 0.8436; MultAcc: 0.7107; BinAcc: 0.8370; 
 	 	 ValLoss: 0.8572; ValMultAcc: 0.7048; ValBinAcc: 0.8379
Step: 100; 58.41.s Loss: 0.8141; AccMul 0.7237 AccBin 0.8417
Step: 200; 116.60.s Loss: 0.8253; AccMul 0.7190 AccBin 0.8405
Step: 300; 174.59.s Loss: 0.8208; AccMul 0.7187 AccBin 0.8417
Step: 400; 232.38.s Loss: 0.8132; AccMul 0.7216 AccBin 0.8440
Step: 500; 289.99.s Loss: 0.8130; AccMul 0.7223 AccBin 0.8447
Step: 600; 347.43.s Loss: 0.8127; AccMul 0.7226 AccBin 0.8451
Step: 700; 404.70.s Loss: 0.8157; AccMul 0.7213 AccBin 0.8435
Step: 800; 461.92.s Loss: 0.8144; AccMul 0.7215 AccBin 0.8438
Step: 900; 519.02.s Loss: 0.8149; AccMul 0.7212 AccBin 0.8438
Step: 1000; 575.91.s Loss: 0.8179; AccMul 0.7209 AccBin 0.8435
Epoch [5/12] Time: 714.66s; BatchTime:0.58s; Loss: 0.8178; MultAcc: 0.7210; BinAcc: 0.8433; 
 	 	 ValLoss: 0.8554; ValMultAcc: 0.7137; ValBinAcc: 0.8398
Step: 100; 57.65.s Loss: 0.7782; AccMul 0.7336 AccBin 0.8485
Step: 200; 115.15.s Loss: 0.7853; AccMul 0.7291 AccBin 0.8465
Step: 300; 172.50.s Loss: 0.7861; AccMul 0.7300 AccBin 0.8459
Step: 400; 229.73.s Loss: 0.7856; AccMul 0.7302 AccBin 0.8467
Step: 500; 286.81.s Loss: 0.7887; AccMul 0.7283 AccBin 0.8458
Step: 600; 343.77.s Loss: 0.7893; AccMul 0.7278 AccBin 0.8457
Step: 700; 400.61.s Loss: 0.7910; AccMul 0.7265 AccBin 0.8452
Step: 800; 457.33.s Loss: 0.7921; AccMul 0.7264 AccBin 0.8446
Step: 900; 513.96.s Loss: 0.7942; AccMul 0.7262 AccBin 0.8446
Step: 1000; 570.47.s Loss: 0.7945; AccMul 0.7261 AccBin 0.8445
Epoch [6/12] Time: 713.32s; BatchTime:0.57s; Loss: 0.7961; MultAcc: 0.7258; BinAcc: 0.8445; 
 	 	 ValLoss: 0.8689; ValMultAcc: 0.6988; ValBinAcc: 0.8344
Step: 100; 57.11.s Loss: 0.7594; AccMul 0.7371 AccBin 0.8482
Step: 200; 114.11.s Loss: 0.7652; AccMul 0.7306 AccBin 0.8438
Step: 300; 171.01.s Loss: 0.7752; AccMul 0.7300 AccBin 0.8441
Step: 400; 227.82.s Loss: 0.7769; AccMul 0.7290 AccBin 0.8446
Step: 500; 284.52.s Loss: 0.7731; AccMul 0.7310 AccBin 0.8462
Step: 600; 341.12.s Loss: 0.7741; AccMul 0.7313 AccBin 0.8466
Step: 700; 397.62.s Loss: 0.7764; AccMul 0.7312 AccBin 0.8469
Step: 800; 454.04.s Loss: 0.7775; AccMul 0.7309 AccBin 0.8468
Step: 900; 510.38.s Loss: 0.7768; AccMul 0.7313 AccBin 0.8472
Step: 1000; 566.62.s Loss: 0.7763; AccMul 0.7323 AccBin 0.8477
Epoch [7/12] Time: 712.80s; BatchTime:0.57s; Loss: 0.7753; MultAcc: 0.7327; BinAcc: 0.8480; 
 	 	 ValLoss: 0.8751; ValMultAcc: 0.7021; ValBinAcc: 0.8328
Step: 100; 56.73.s Loss: 0.7354; AccMul 0.7436 AccBin 0.8497
Step: 200; 113.37.s Loss: 0.7487; AccMul 0.7389 AccBin 0.8487
Step: 300; 169.93.s Loss: 0.7531; AccMul 0.7383 AccBin 0.8490
Step: 400; 226.40.s Loss: 0.7545; AccMul 0.7401 AccBin 0.8510
Step: 500; 282.80.s Loss: 0.7536; AccMul 0.7394 AccBin 0.8509
Step: 600; 339.12.s Loss: 0.7545; AccMul 0.7377 AccBin 0.8503
Step: 700; 395.39.s Loss: 0.7540; AccMul 0.7380 AccBin 0.8508
Step: 800; 451.60.s Loss: 0.7547; AccMul 0.7380 AccBin 0.8512
Step: 900; 507.76.s Loss: 0.7563; AccMul 0.7375 AccBin 0.8509
Step: 1000; 563.83.s Loss: 0.7562; AccMul 0.7378 AccBin 0.8507
Epoch [8/12] Time: 714.65s; BatchTime:0.56s; Loss: 0.7589; MultAcc: 0.7369; BinAcc: 0.8503; 
 	 	 ValLoss: 0.8683; ValMultAcc: 0.7169; ValBinAcc: 0.8390
Step: 100; 56.45.s Loss: 0.7112; AccMul 0.7477 AccBin 0.8558
Step: 200; 112.84.s Loss: 0.7135; AccMul 0.7484 AccBin 0.8564
Step: 300; 169.16.s Loss: 0.7265; AccMul 0.7460 AccBin 0.8539
Step: 400; 225.43.s Loss: 0.7330; AccMul 0.7436 AccBin 0.8524
Step: 500; 281.64.s Loss: 0.7371; AccMul 0.7429 AccBin 0.8527
Step: 600; 337.80.s Loss: 0.7408; AccMul 0.7429 AccBin 0.8528
Step: 700; 393.91.s Loss: 0.7413; AccMul 0.7421 AccBin 0.8530
Step: 800; 449.96.s Loss: 0.7416; AccMul 0.7421 AccBin 0.8527
Step: 900; 505.95.s Loss: 0.7410; AccMul 0.7430 AccBin 0.8535
Step: 1000; 561.89.s Loss: 0.7425; AccMul 0.7425 AccBin 0.8532
Epoch [9/12] Time: 716.54s; BatchTime:0.56s; Loss: 0.7436; MultAcc: 0.7421; BinAcc: 0.8532; 
 	 	 ValLoss: 0.8641; ValMultAcc: 0.7087; ValBinAcc: 0.8393
Step: 100; 56.25.s Loss: 0.7098; AccMul 0.7484 AccBin 0.8578
Step: 200; 112.46.s Loss: 0.7080; AccMul 0.7485 AccBin 0.8586
Step: 300; 168.61.s Loss: 0.7174; AccMul 0.7461 AccBin 0.8562
Step: 400; 224.70.s Loss: 0.7213; AccMul 0.7453 AccBin 0.8551
Step: 500; 280.75.s Loss: 0.7236; AccMul 0.7452 AccBin 0.8545
Step: 600; 336.75.s Loss: 0.7260; AccMul 0.7439 AccBin 0.8539
Step: 700; 392.72.s Loss: 0.7257; AccMul 0.7441 AccBin 0.8545
Step: 800; 448.64.s Loss: 0.7248; AccMul 0.7443 AccBin 0.8540
Step: 900; 504.51.s Loss: 0.7219; AccMul 0.7457 AccBin 0.8549
Step: 1000; 560.35.s Loss: 0.7215; AccMul 0.7465 AccBin 0.8550
Epoch [10/12] Time: 716.73s; BatchTime:0.56s; Loss: 0.7226; MultAcc: 0.7465; BinAcc: 0.8549; 
 	 	 ValLoss: 0.8755; ValMultAcc: 0.7151; ValBinAcc: 0.8376
Step: 100; 56.09.s Loss: 0.7018; AccMul 0.7554 AccBin 0.8590
Step: 200; 112.14.s Loss: 0.7130; AccMul 0.7498 AccBin 0.8553
Step: 300; 168.15.s Loss: 0.7066; AccMul 0.7520 AccBin 0.8567
Step: 400; 224.11.s Loss: 0.7096; AccMul 0.7507 AccBin 0.8566
Step: 500; 280.03.s Loss: 0.7144; AccMul 0.7490 AccBin 0.8563
Step: 600; 335.90.s Loss: 0.7110; AccMul 0.7498 AccBin 0.8568
Step: 700; 391.73.s Loss: 0.7104; AccMul 0.7495 AccBin 0.8563
Step: 800; 447.53.s Loss: 0.7102; AccMul 0.7492 AccBin 0.8559
Step: 900; 503.30.s Loss: 0.7112; AccMul 0.7493 AccBin 0.8561
Step: 1000; 559.04.s Loss: 0.7116; AccMul 0.7492 AccBin 0.8557
Epoch [11/12] Time: 716.32s; BatchTime:0.56s; Loss: 0.7117; MultAcc: 0.7492; BinAcc: 0.8556; 
 	 	 ValLoss: 0.8726; ValMultAcc: 0.7131; ValBinAcc: 0.8392
Step: 100; 55.98.s Loss: 0.6725; AccMul 0.7594 AccBin 0.8591
Step: 200; 111.91.s Loss: 0.6752; AccMul 0.7569 AccBin 0.8588
Step: 300; 167.81.s Loss: 0.6779; AccMul 0.7564 AccBin 0.8597
Step: 400; 223.70.s Loss: 0.6830; AccMul 0.7557 AccBin 0.8587
Step: 500; 279.63.s Loss: 0.6829; AccMul 0.7563 AccBin 0.8595
Step: 600; 335.55.s Loss: 0.6850; AccMul 0.7558 AccBin 0.8592
Step: 700; 391.47.s Loss: 0.6875; AccMul 0.7555 AccBin 0.8591
Step: 800; 447.45.s Loss: 0.6922; AccMul 0.7543 AccBin 0.8583
Step: 900; 503.25.s Loss: 0.6972; AccMul 0.7526 AccBin 0.8568
Step: 1000; 558.98.s Loss: 0.6986; AccMul 0.7527 AccBin 0.8568
Epoch [12/12] Time: 736.46s; BatchTime:0.56s; Loss: 0.6981; MultAcc: 0.7530; BinAcc: 0.8569; 
 	 	 ValLoss: 0.8774; ValMultAcc: 0.7120; ValBinAcc: 0.8350

TEST Loss: 0.8629; AccMult: 0.7098; AccBin: 0.8413

ROC AUC SCORE
	 good: 0.8865
	 celeb: 0.8900
	 casino: 0.8883
	 guns: 0.8814
	 illegal: 0.8095
	 narco: 0.7693
	 porno: 0.9036
	 yellow: 0.8205
Peak memory usage by Pytorch tensors: 1298.18 Mb
