Logging Timestamp 2020-03-19 20.07.32

Namespace(batch=100, batch_count=1, checkpoint=-1, data_dir='data', epochs=10, memory_usage=True, model='resnet50', no_scheduler=False, num_workers=4, opt_lr=0.01, out='models/model_resnet50_2020-03-19 20.07.32.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model require grad = False
device cuda:0
Adaptive
Combine loss alpha=0.5
Batch size 100
Classes: good celebrity casino guns illegal narcotics porno yellow
Run training

Step: 100; 72.10.s Loss: 1.4327; AccMul 0.6333 AccBin 0.7511
Step: 200; 132.76.s Loss: 1.0605; AccMul 0.6782 AccBin 0.7853
Step: 300; 193.12.s Loss: 0.9362; AccMul 0.6976 AccBin 0.7997
Step: 400; 254.49.s Loss: 0.8793; AccMul 0.7067 AccBin 0.8077
Step: 500; 316.12.s Loss: 0.8583; AccMul 0.7101 AccBin 0.8096
Epoch [1/10] Time: 416.34s; BatchTime:0.63s; Loss: 0.8503; MultAcc: 0.7113; BinAcc: 0.8113; 
 	 	 ValLoss: 0.7254; ValMultAcc: 0.7383; ValBinAcc: 0.8122
Learning rate: [0.01]; Count: 5
Step: 100; 63.82.s Loss: 0.8121; AccMul 0.7199 AccBin 0.8165
Step: 200; 125.82.s Loss: 0.7088; AccMul 0.7397 AccBin 0.8318
Step: 300; 186.85.s Loss: 0.6744; AccMul 0.7470 AccBin 0.8346
Step: 400; 248.44.s Loss: 0.6569; AccMul 0.7507 AccBin 0.8367
Step: 500; 310.28.s Loss: 0.6448; AccMul 0.7532 AccBin 0.8384
Epoch [2/10] Time: 401.00s; BatchTime:0.62s; Loss: 0.6409; MultAcc: 0.7543; BinAcc: 0.8391; 
 	 	 ValLoss: 0.5804; ValMultAcc: 0.7684; ValBinAcc: 0.8581
Learning rate: [0.0025]; Count: 10
Step: 100; 62.43.s Loss: 0.5892; AccMul 0.7666 AccBin 0.8393
Step: 200; 124.15.s Loss: 0.5796; AccMul 0.7732 AccBin 0.8469
Step: 300; 185.33.s Loss: 0.5727; AccMul 0.7763 AccBin 0.8506
Step: 400; 246.10.s Loss: 0.5680; AccMul 0.7781 AccBin 0.8524
Step: 500; 306.49.s Loss: 0.5619; AccMul 0.7807 AccBin 0.8546
Epoch [3/10] Time: 395.03s; BatchTime:0.61s; Loss: 0.5612; MultAcc: 0.7803; BinAcc: 0.8547; 
 	 	 ValLoss: 0.5385; ValMultAcc: 0.7987; ValBinAcc: 0.8631
Learning rate: [0.00125]; Count: 15
Step: 100; 61.70.s Loss: 0.5803; AccMul 0.7700 AccBin 0.8475
Step: 200; 123.25.s Loss: 0.5652; AccMul 0.7784 AccBin 0.8531
Step: 300; 184.84.s Loss: 0.5645; AccMul 0.7784 AccBin 0.8531
Step: 400; 247.36.s Loss: 0.5599; AccMul 0.7815 AccBin 0.8549
Step: 500; 310.25.s Loss: 0.5523; AccMul 0.7864 AccBin 0.8583
Epoch [4/10] Time: 428.12s; BatchTime:0.62s; Loss: 0.5518; MultAcc: 0.7871; BinAcc: 0.8588; 
 	 	 ValLoss: 0.5317; ValMultAcc: 0.8033; ValBinAcc: 0.8735
Learning rate: [0.000625]; Count: 20
Step: 100; 62.60.s Loss: 0.5467; AccMul 0.7899 AccBin 0.8668
Step: 200; 125.15.s Loss: 0.5338; AccMul 0.7981 AccBin 0.8707
Step: 300; 187.67.s Loss: 0.5286; AccMul 0.8002 AccBin 0.8708
Step: 400; 250.14.s Loss: 0.5235; AccMul 0.8035 AccBin 0.8722
Step: 500; 312.58.s Loss: 0.5204; AccMul 0.8054 AccBin 0.8727
Epoch [5/10] Time: 418.91s; BatchTime:0.62s; Loss: 0.5199; MultAcc: 0.8053; BinAcc: 0.8729; 
 	 	 ValLoss: 0.5187; ValMultAcc: 0.8072; ValBinAcc: 0.8734
Learning rate: [0.0003125]; Count: 25
Step: 100; 62.93.s Loss: 0.4774; AccMul 0.8243 AccBin 0.8820
Step: 200; 126.06.s Loss: 0.4751; AccMul 0.8259 AccBin 0.8850
Step: 300; 189.63.s Loss: 0.4742; AccMul 0.8267 AccBin 0.8863
Step: 400; 253.29.s Loss: 0.4726; AccMul 0.8283 AccBin 0.8877
Step: 500; 316.98.s Loss: 0.4753; AccMul 0.8269 AccBin 0.8867
Epoch [6/10] Time: 434.68s; BatchTime:0.63s; Loss: 0.4774; MultAcc: 0.8254; BinAcc: 0.8858; 
 	 	 ValLoss: 0.5129; ValMultAcc: 0.8141; ValBinAcc: 0.8826
Learning rate: [0.00015625]; Count: 30
Step: 100; 63.57.s Loss: 0.4332; AccMul 0.8498 AccBin 0.9029
Step: 200; 126.94.s Loss: 0.4333; AccMul 0.8509 AccBin 0.9044
Step: 300; 190.95.s Loss: 0.4353; AccMul 0.8504 AccBin 0.9042
Step: 400; 255.36.s Loss: 0.4386; AccMul 0.8481 AccBin 0.9025
Step: 500; 320.08.s Loss: 0.4419; AccMul 0.8461 AccBin 0.9003
Epoch [7/10] Time: 441.96s; BatchTime:0.64s; Loss: 0.4421; MultAcc: 0.8459; BinAcc: 0.8998; 
 	 	 ValLoss: 0.5036; ValMultAcc: 0.8211; ValBinAcc: 0.8835
Learning rate: [7.8125e-05]; Count: 35
Step: 100; 64.58.s Loss: 0.3924; AccMul 0.8697 AccBin 0.9179
Step: 200; 129.68.s Loss: 0.3923; AccMul 0.8706 AccBin 0.9185
Step: 300; 195.26.s Loss: 0.3906; AccMul 0.8726 AccBin 0.9194
Step: 400; 261.28.s Loss: 0.3932; AccMul 0.8712 AccBin 0.9188
Step: 500; 327.72.s Loss: 0.3932; AccMul 0.8716 AccBin 0.9189
Epoch [8/10] Time: 486.41s; BatchTime:0.66s; Loss: 0.3935; MultAcc: 0.8714; BinAcc: 0.9190; 
 	 	 ValLoss: 0.5125; ValMultAcc: 0.8298; ValBinAcc: 0.8874
Learning rate: [3.90625e-05]; Count: 40
Step: 100; 66.04.s Loss: 0.3478; AccMul 0.8966 AccBin 0.9354
Step: 200; 132.47.s Loss: 0.3461; AccMul 0.8993 AccBin 0.9371
Step: 300; 199.24.s Loss: 0.3454; AccMul 0.8991 AccBin 0.9370
Step: 400; 266.33.s Loss: 0.3460; AccMul 0.8990 AccBin 0.9370
Step: 500; 333.74.s Loss: 0.3462; AccMul 0.8988 AccBin 0.9372
Epoch [9/10] Time: 480.05s; BatchTime:0.67s; Loss: 0.3466; MultAcc: 0.8984; BinAcc: 0.9365; 
 	 	 ValLoss: 0.5292; ValMultAcc: 0.8264; ValBinAcc: 0.8856
Learning rate: [1.953125e-05]; Count: 45
Step: 100; 67.07.s Loss: 0.3135; AccMul 0.9148 AccBin 0.9464
Step: 200; 134.32.s Loss: 0.3166; AccMul 0.9129 AccBin 0.9464
Step: 300; 201.87.s Loss: 0.3166; AccMul 0.9117 AccBin 0.9463
Step: 400; 269.52.s Loss: 0.3159; AccMul 0.9124 AccBin 0.9469
Step: 500; 337.29.s Loss: 0.3165; AccMul 0.9122 AccBin 0.9467
Epoch [10/10] Time: 464.30s; BatchTime:0.67s; Loss: 0.3168; MultAcc: 0.9122; BinAcc: 0.9465; 
 	 	 ValLoss: 0.5433; ValMultAcc: 0.8240; ValBinAcc: 0.8852
Learning rate: [9.765625e-06]; Count: 50

TEST Loss: 0.5167; AccMult: 0.8301; AccBin: 0.8875

ROC AUC SCORE
	 good: 0.9395
	 celebrity: 0.9630
	 casino: 0.9045
	 guns: 0.9352
	 illegal: 0.8796
	 narcotics: 0.8140
	 porno: 0.9639
	 yellow: 0.8526
Peak memory usage by Pytorch tensors: 2312.71 Mb
