Logging Timestamp 2020-05-04 01.53.50

Namespace(adaptive=False, batch=100, batch_count=1, checkpoint=-1, data_dir='data_new', epochs=5, memory_usage=True, model='dense121', no_scheduler=False, num_workers=4, opt_lr=0.001, out='models/model_dense121_2020-05-04 01.53.50.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model dense121 require grad = False
device cuda:0
Adaptive
Weights [0.5, 1, 1, 1, 1, 1, 1, 0.5]
Batch size 100
Classes: good celeb casino guns illegal narco porno yellow
Run training

Step: 100; 67.04.s Loss: 1.2964; AccMul 0.5868 AccBin 0.7660
Step: 200; 121.00.s Loss: 1.1733; AccMul 0.6203 AccBin 0.7863
Step: 300; 175.00.s Loss: 1.1159; AccMul 0.6352 AccBin 0.7970
Step: 400; 229.22.s Loss: 1.0857; AccMul 0.6440 AccBin 0.8017
Step: 500; 283.40.s Loss: 1.0641; AccMul 0.6490 AccBin 0.8046
Step: 600; 337.56.s Loss: 1.0467; AccMul 0.6551 AccBin 0.8080
Step: 700; 391.73.s Loss: 1.0341; AccMul 0.6580 AccBin 0.8090
Step: 800; 445.90.s Loss: 1.0224; AccMul 0.6609 AccBin 0.8108
Step: 900; 500.37.s Loss: 1.0102; AccMul 0.6647 AccBin 0.8132
Step: 1000; 559.70.s Loss: 1.0029; AccMul 0.6674 AccBin 0.8147
Epoch [1/5] Time: 747.78s; BatchTime:0.56s; Loss: 0.9988; MultAcc: 0.6684; BinAcc: 0.8152; 
 	 	 ValLoss: 0.8950; ValMultAcc: 0.6832; ValBinAcc: 0.8282
Step: 100; 57.07.s Loss: 0.8754; AccMul 0.6966 AccBin 0.8309
Step: 200; 114.01.s Loss: 0.8923; AccMul 0.6951 AccBin 0.8321
Step: 300; 170.41.s Loss: 0.8919; AccMul 0.6969 AccBin 0.8298
Step: 400; 226.53.s Loss: 0.8906; AccMul 0.6973 AccBin 0.8294
Step: 500; 282.41.s Loss: 0.8897; AccMul 0.6983 AccBin 0.8299
Step: 600; 338.14.s Loss: 0.8912; AccMul 0.6979 AccBin 0.8302
Step: 700; 393.67.s Loss: 0.8942; AccMul 0.6972 AccBin 0.8302
Step: 800; 449.09.s Loss: 0.8932; AccMul 0.6977 AccBin 0.8303
Step: 900; 504.43.s Loss: 0.8935; AccMul 0.6984 AccBin 0.8304
Step: 1000; 559.86.s Loss: 0.8935; AccMul 0.6985 AccBin 0.8308
Epoch [2/5] Time: 730.06s; BatchTime:0.56s; Loss: 0.8918; MultAcc: 0.6987; BinAcc: 0.8311; 
 	 	 ValLoss: 0.8699; ValMultAcc: 0.7107; ValBinAcc: 0.8417
Step: 100; 56.29.s Loss: 0.8642; AccMul 0.7121 AccBin 0.8384
Step: 200; 112.43.s Loss: 0.8550; AccMul 0.7109 AccBin 0.8389
Step: 300; 168.32.s Loss: 0.8501; AccMul 0.7108 AccBin 0.8390
Step: 400; 224.06.s Loss: 0.8497; AccMul 0.7122 AccBin 0.8383
Step: 500; 279.90.s Loss: 0.8517; AccMul 0.7116 AccBin 0.8389
Step: 600; 334.93.s Loss: 0.8540; AccMul 0.7111 AccBin 0.8381
Step: 700; 389.59.s Loss: 0.8552; AccMul 0.7114 AccBin 0.8385
Step: 800; 445.45.s Loss: 0.8554; AccMul 0.7109 AccBin 0.8378
Step: 900; 500.49.s Loss: 0.8547; AccMul 0.7107 AccBin 0.8378
Step: 1000; 555.02.s Loss: 0.8546; AccMul 0.7103 AccBin 0.8373
Epoch [3/5] Time: 719.73s; BatchTime:0.55s; Loss: 0.8534; MultAcc: 0.7104; BinAcc: 0.8373; 
 	 	 ValLoss: 0.8701; ValMultAcc: 0.7081; ValBinAcc: 0.8334
Step: 100; 55.77.s Loss: 0.8010; AccMul 0.7284 AccBin 0.8474
Step: 200; 111.48.s Loss: 0.8082; AccMul 0.7275 AccBin 0.8447
Step: 300; 167.15.s Loss: 0.8179; AccMul 0.7212 AccBin 0.8408
Step: 400; 222.82.s Loss: 0.8161; AccMul 0.7216 AccBin 0.8411
Step: 500; 278.71.s Loss: 0.8174; AccMul 0.7204 AccBin 0.8402
Step: 600; 334.71.s Loss: 0.8195; AccMul 0.7202 AccBin 0.8406
Step: 700; 390.88.s Loss: 0.8204; AccMul 0.7204 AccBin 0.8415
Step: 800; 447.64.s Loss: 0.8230; AccMul 0.7195 AccBin 0.8420
Step: 900; 504.72.s Loss: 0.8241; AccMul 0.7196 AccBin 0.8422
Step: 1000; 561.81.s Loss: 0.8243; AccMul 0.7194 AccBin 0.8424
Epoch [4/5] Time: 763.04s; BatchTime:0.56s; Loss: 0.8254; MultAcc: 0.7190; BinAcc: 0.8423; 
 	 	 ValLoss: 0.8510; ValMultAcc: 0.7131; ValBinAcc: 0.8421
Step: 100; 56.51.s Loss: 0.8161; AccMul 0.7256 AccBin 0.8443
Step: 200; 113.20.s Loss: 0.7987; AccMul 0.7294 AccBin 0.8435
Step: 300; 169.71.s Loss: 0.8036; AccMul 0.7281 AccBin 0.8442
Step: 400; 226.11.s Loss: 0.8064; AccMul 0.7243 AccBin 0.8425
Step: 500; 282.44.s Loss: 0.8067; AccMul 0.7238 AccBin 0.8431
Step: 600; 338.69.s Loss: 0.8049; AccMul 0.7234 AccBin 0.8423
Step: 700; 394.88.s Loss: 0.8020; AccMul 0.7241 AccBin 0.8432
Step: 800; 450.99.s Loss: 0.8031; AccMul 0.7242 AccBin 0.8433
Step: 900; 507.05.s Loss: 0.8036; AccMul 0.7243 AccBin 0.8431
Step: 1000; 563.06.s Loss: 0.8046; AccMul 0.7240 AccBin 0.8430
Epoch [5/5] Time: 739.34s; BatchTime:0.56s; Loss: 0.8061; MultAcc: 0.7235; BinAcc: 0.8426; 
 	 	 ValLoss: 0.8784; ValMultAcc: 0.7164; ValBinAcc: 0.8410

TEST Loss: 0.8586; AccMult: 0.7031; AccBin: 0.8356

ROC AUC SCORE
	 good: 0.8758
	 celeb: 0.8981
	 casino: 0.8562
	 guns: 0.8913
	 illegal: 0.8525
	 narco: 0.8135
	 porno: 0.8995
	 yellow: 0.8104
Peak memory usage by Pytorch tensors: 1125.83 Mb
