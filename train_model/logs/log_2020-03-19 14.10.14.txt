Logging Timestamp 2020-03-19 14.10.14

Namespace(batch=50, batch_count=1, checkpoint=-1, data_dir='data', epochs=8, memory_usage=True, model='resnet50', num_workers=4, opt_lr=0.01, out='models/model_resnet50_2020-03-19 14.10.14.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model require grad = False
device cuda:0
Batch size 50
Classes: good celebrity casino guns illegal porno yellow
Run training

Step: 200; 61.14.s Loss: 1.3484; AccMul 0.6784 AccBin 0.7838
Step: 400; 112.35.s Loss: 1.2459; AccMul 0.6919 AccBin 0.7935
Step: 600; 163.75.s Loss: 1.2102; AccMul 0.6969 AccBin 0.7976
Step: 800; 214.53.s Loss: 1.2155; AccMul 0.6979 AccBin 0.7990
Step: 1000; 265.49.s Loss: 1.2211; AccMul 0.6985 AccBin 0.8007
Epoch [1/8] Time: 350.35s; BatchTime:0.26s; Loss: 1.2252; MultAcc: 0.6981; BinAcc: 0.8003; 
 	 	 ValLoss: 1.1494; ValMultAcc: 0.7298; ValBinAcc: 0.8431
Learning rate: [0.0025]; Count: 5
Step: 200; 54.25.s Loss: 1.2173; AccMul 0.7171 AccBin 0.8047
Step: 400; 107.84.s Loss: 0.9956; AccMul 0.7334 AccBin 0.8174
Step: 600; 161.14.s Loss: 0.9081; AccMul 0.7402 AccBin 0.8236
Step: 800; 214.17.s Loss: 0.8558; AccMul 0.7461 AccBin 0.8267
Step: 1000; 266.91.s Loss: 0.8234; AccMul 0.7510 AccBin 0.8298
Epoch [2/8] Time: 354.56s; BatchTime:0.27s; Loss: 0.8167; MultAcc: 0.7519; BinAcc: 0.8306; 
 	 	 ValLoss: 0.6848; ValMultAcc: 0.7818; ValBinAcc: 0.8535
Learning rate: [0.00125]; Count: 10
Step: 200; 54.14.s Loss: 0.6808; AccMul 0.7681 AccBin 0.8405
Step: 400; 108.26.s Loss: 0.6561; AccMul 0.7799 AccBin 0.8472
Step: 600; 162.37.s Loss: 0.6496; AccMul 0.7818 AccBin 0.8483
Step: 800; 216.44.s Loss: 0.6459; AccMul 0.7842 AccBin 0.8505
Step: 1000; 270.45.s Loss: 0.6476; AccMul 0.7826 AccBin 0.8498
Epoch [3/8] Time: 369.89s; BatchTime:0.27s; Loss: 0.6450; MultAcc: 0.7836; BinAcc: 0.8505; 
 	 	 ValLoss: 0.6089; ValMultAcc: 0.8020; ValBinAcc: 0.8622
Learning rate: [0.000625]; Count: 15
Step: 200; 55.02.s Loss: 0.7137; AccMul 0.7638 AccBin 0.8379
Step: 400; 110.15.s Loss: 0.6752; AccMul 0.7753 AccBin 0.8458
Step: 600; 165.40.s Loss: 0.6578; AccMul 0.7807 AccBin 0.8502
Step: 800; 220.72.s Loss: 0.6470; AccMul 0.7836 AccBin 0.8514
Step: 1000; 276.97.s Loss: 0.6414; AccMul 0.7848 AccBin 0.8522
Epoch [4/8] Time: 391.32s; BatchTime:0.28s; Loss: 0.6394; MultAcc: 0.7858; BinAcc: 0.8527; 
 	 	 ValLoss: 0.6090; ValMultAcc: 0.8042; ValBinAcc: 0.8683
Learning rate: [0.0003125]; Count: 20
Step: 200; 56.22.s Loss: 0.6247; AccMul 0.7903 AccBin 0.8550
Step: 400; 113.02.s Loss: 0.6062; AccMul 0.7961 AccBin 0.8601
Step: 600; 170.35.s Loss: 0.6000; AccMul 0.7983 AccBin 0.8608
Step: 800; 228.17.s Loss: 0.5943; AccMul 0.7999 AccBin 0.8612
Step: 1000; 286.37.s Loss: 0.5897; AccMul 0.8015 AccBin 0.8624
Epoch [5/8] Time: 419.11s; BatchTime:0.29s; Loss: 0.5897; MultAcc: 0.8013; BinAcc: 0.8627; 
 	 	 ValLoss: 0.5672; ValMultAcc: 0.8143; ValBinAcc: 0.8810
Learning rate: [0.00015625]; Count: 25
Step: 200; 57.99.s Loss: 0.5193; AccMul 0.8252 AccBin 0.8770
Step: 400; 116.53.s Loss: 0.5237; AccMul 0.8239 AccBin 0.8793
Step: 600; 175.57.s Loss: 0.5280; AccMul 0.8221 AccBin 0.8781
Step: 800; 235.06.s Loss: 0.5290; AccMul 0.8219 AccBin 0.8789
Step: 1000; 294.94.s Loss: 0.5290; AccMul 0.8219 AccBin 0.8786
Epoch [6/8] Time: 433.67s; BatchTime:0.30s; Loss: 0.5283; MultAcc: 0.8227; BinAcc: 0.8791; 
 	 	 ValLoss: 0.5446; ValMultAcc: 0.8205; ValBinAcc: 0.8782
Learning rate: [7.8125e-05]; Count: 30
Step: 200; 59.74.s Loss: 0.4795; AccMul 0.8366 AccBin 0.8885
Step: 400; 120.28.s Loss: 0.4816; AccMul 0.8366 AccBin 0.8873
Step: 600; 181.50.s Loss: 0.4858; AccMul 0.8359 AccBin 0.8869
Step: 800; 243.24.s Loss: 0.4858; AccMul 0.8361 AccBin 0.8873
Step: 1000; 305.54.s Loss: 0.4834; AccMul 0.8363 AccBin 0.8878
Epoch [7/8] Time: 466.79s; BatchTime:0.31s; Loss: 0.4839; MultAcc: 0.8360; BinAcc: 0.8875; 
 	 	 ValLoss: 0.5470; ValMultAcc: 0.8264; ValBinAcc: 0.8864
Learning rate: [3.90625e-05]; Count: 35
Step: 200; 61.79.s Loss: 0.4032; AccMul 0.8624 AccBin 0.9056
Step: 400; 124.30.s Loss: 0.4008; AccMul 0.8646 AccBin 0.9067
Step: 600; 187.57.s Loss: 0.4007; AccMul 0.8632 AccBin 0.9068
Step: 800; 251.46.s Loss: 0.4011; AccMul 0.8638 AccBin 0.9068
Step: 1000; 315.91.s Loss: 0.4025; AccMul 0.8639 AccBin 0.9067
Epoch [8/8] Time: 488.89s; BatchTime:0.32s; Loss: 0.4038; MultAcc: 0.8635; BinAcc: 0.9066; 
 	 	 ValLoss: 0.5524; ValMultAcc: 0.8309; ValBinAcc: 0.8878
Learning rate: [1.953125e-05]; Count: 40

TEST Loss: 0.5250; AccMult: 0.8312; AccBin: 0.8863

ROC AUC SCORE
	 good: 0.9461
	 celebrity: 0.9660
	 casino: 0.9207
	 guns: 0.9454
	 illegal: 0.8813
	 porno: 0.9573
	 yellow: 0.8670
Peak memory usage by Pytorch tensors: 1134.16 Mb
