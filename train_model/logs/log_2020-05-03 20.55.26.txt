Logging Timestamp 2020-05-03 20.55.26

Namespace(adaptive=False, batch=50, batch_count=1, checkpoint=-1, data_dir='data_new', epochs=1, memory_usage=True, model='vgg16_bn', no_scheduler=False, num_workers=4, opt_lr=0.001, out='models/model_vgg16_bn_2020-05-03 20.55.26.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model vgg16_bn require grad = False
device cuda:0
Adaptive
Weights [0.5, 1, 1, 1, 1, 1, 1, 0.5]
Batch size 50
Classes: good celeb casino guns illegal narco porno yellow
Run training

Step: 100; 68.42.s Loss: 1.7059; AccMul 0.4978 AccBin 0.7002
Step: 200; 123.45.s Loss: 1.5339; AccMul 0.5288 AccBin 0.7296
Step: 300; 178.47.s Loss: 1.4884; AccMul 0.5379 AccBin 0.7387
Step: 400; 233.48.s Loss: 1.4512; AccMul 0.5440 AccBin 0.7473
Step: 500; 288.48.s Loss: 1.4364; AccMul 0.5472 AccBin 0.7495
Step: 600; 343.58.s Loss: 1.4253; AccMul 0.5482 AccBin 0.7519
Step: 700; 398.63.s Loss: 1.4185; AccMul 0.5507 AccBin 0.7542
Step: 800; 453.86.s Loss: 1.4118; AccMul 0.5522 AccBin 0.7551
Step: 900; 510.40.s Loss: 1.4050; AccMul 0.5541 AccBin 0.7560
Step: 1000; 566.15.s Loss: 1.4044; AccMul 0.5542 AccBin 0.7561
Step: 1100; 621.60.s Loss: 1.4018; AccMul 0.5553 AccBin 0.7564
Step: 1200; 677.06.s Loss: 1.3984; AccMul 0.5559 AccBin 0.7568
Step: 1300; 732.66.s Loss: 1.3947; AccMul 0.5567 AccBin 0.7570
Step: 1400; 788.38.s Loss: 1.3886; AccMul 0.5583 AccBin 0.7578
Step: 1500; 844.12.s Loss: 1.3856; AccMul 0.5595 AccBin 0.7581
Step: 1600; 899.58.s Loss: 1.3829; AccMul 0.5602 AccBin 0.7587
Step: 1700; 955.04.s Loss: 1.3774; AccMul 0.5619 AccBin 0.7597
Step: 1800; 1010.50.s Loss: 1.3747; AccMul 0.5627 AccBin 0.7607
Step: 1900; 1066.59.s Loss: 1.3720; AccMul 0.5636 AccBin 0.7611
Step: 2000; 1123.58.s Loss: 1.3680; AccMul 0.5652 AccBin 0.7619
Step: 2100; 1179.02.s Loss: 1.3636; AccMul 0.5672 AccBin 0.7633
Epoch [1/1] Time: 1434.95s; BatchTime:0.56s; Loss: 1.3633; MultAcc: 0.5673; BinAcc: 0.7634; 
 	 	 ValLoss: 1.1053; ValMultAcc: 0.6530; ValBinAcc: 0.8051

TEST Loss: 1.1193; AccMult: 0.6485; AccBin: 0.8031

ROC AUC SCORE
	 good: 0.7128
	 celeb: 0.7418
	 casino: 0.7525
	 guns: 0.7922
	 illegal: 0.6616
	 narco: 0.6767
	 porno: 0.7181
	 yellow: 0.6474
Peak memory usage by Pytorch tensors: 3142.16 Mb
