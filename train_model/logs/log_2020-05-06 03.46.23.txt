Logging Timestamp 2020-05-06 03.46.23

Namespace(adaptive=True, batch=100, batch_count=1, checkpoint=-1, data_dir='data_new', epochs=10, memory_usage=True, model='resnet50', no_scheduler=False, num_workers=4, opt_lr=0.01, out='models/model_resnet50_2020-05-06 03.46.23.pth', plot_hist=True, save_best=True, stop_accuracy=0.4, teacher_file='')
Model resnet50 require grad = False
device cuda:0
Adaptive
Weights [0.5, 1, 1, 1, 1, 1, 1, 0.5]
Optim Adam
LR: 0.01 0.5
Batch size 100
Classes: good celeb casino guns illegal narco porno yellow
Added narco x2
Run training

Step: 100; 67.72.s Loss: 2.3279; AccMul 0.5014 AccBin 0.7304
Step: 200; 123.06.s Loss: 1.7975; AccMul 0.5431 AccBin 0.7567
Step: 300; 178.45.s Loss: 1.6357; AccMul 0.5583 AccBin 0.7659
Step: 400; 233.98.s Loss: 1.5630; AccMul 0.5637 AccBin 0.7678
Step: 500; 285.47.s Loss: 1.5235; AccMul 0.5668 AccBin 0.7693
Step: 600; 342.44.s Loss: 1.4976; AccMul 0.5699 AccBin 0.7713
Step: 700; 403.58.s Loss: 1.4794; AccMul 0.5725 AccBin 0.7734
Step: 800; 465.53.s Loss: 1.4695; AccMul 0.5741 AccBin 0.7741
Step: 900; 526.41.s Loss: 1.4709; AccMul 0.5740 AccBin 0.7730
Step: 1000; 587.23.s Loss: 1.4693; AccMul 0.5754 AccBin 0.7743
Epoch [1/10] Time: 791.02s; BatchTime:0.59s; Loss: 1.4734; MultAcc: 0.5753; BinAcc: 0.7744; 
 	 	 ValLoss: 1.4302; ValMultAcc: 0.5694; ValBinAcc: 0.7868
Count: 10
Step: 100; 59.68.s Loss: 1.4602; AccMul 0.5874 AccBin 0.7840
Step: 200; 119.14.s Loss: 1.2632; AccMul 0.6145 AccBin 0.7953
Step: 300; 178.34.s Loss: 1.1990; AccMul 0.6233 AccBin 0.8009
Step: 400; 237.36.s Loss: 1.1626; AccMul 0.6296 AccBin 0.8042
Step: 500; 296.23.s Loss: 1.1380; AccMul 0.6340 AccBin 0.8053
Step: 600; 355.04.s Loss: 1.1168; AccMul 0.6384 AccBin 0.8062
Step: 700; 413.73.s Loss: 1.1012; AccMul 0.6424 AccBin 0.8074
Step: 800; 472.32.s Loss: 1.0873; AccMul 0.6447 AccBin 0.8081
Step: 900; 530.84.s Loss: 1.0789; AccMul 0.6467 AccBin 0.8087
Step: 1000; 589.24.s Loss: 1.0674; AccMul 0.6496 AccBin 0.8100
Epoch [2/10] Time: 777.37s; BatchTime:0.59s; Loss: 1.0631; MultAcc: 0.6502; BinAcc: 0.8099; 
 	 	 ValLoss: 0.9778; ValMultAcc: 0.6535; ValBinAcc: 0.8159
Count: 15
Step: 100; 59.40.s Loss: 1.1327; AccMul 0.6298 AccBin 0.8069
Step: 200; 118.89.s Loss: 1.1000; AccMul 0.6346 AccBin 0.8034
Step: 300; 178.42.s Loss: 1.0599; AccMul 0.6483 AccBin 0.8108
Step: 400; 238.41.s Loss: 1.0368; AccMul 0.6551 AccBin 0.8134
Step: 500; 298.12.s Loss: 1.0304; AccMul 0.6577 AccBin 0.8147
Step: 600; 357.88.s Loss: 1.0162; AccMul 0.6623 AccBin 0.8164
Step: 700; 417.65.s Loss: 1.0076; AccMul 0.6647 AccBin 0.8172
Step: 800; 477.44.s Loss: 0.9965; AccMul 0.6684 AccBin 0.8189
Step: 900; 537.25.s Loss: 0.9877; AccMul 0.6712 AccBin 0.8198
Step: 1000; 597.09.s Loss: 0.9819; AccMul 0.6726 AccBin 0.8199
Epoch [3/10] Time: 803.50s; BatchTime:0.60s; Loss: 0.9781; MultAcc: 0.6736; BinAcc: 0.8203; 
 	 	 ValLoss: 0.8958; ValMultAcc: 0.7025; ValBinAcc: 0.8294
Count: 20
Step: 100; 60.09.s Loss: 0.9551; AccMul 0.6840 AccBin 0.8261
Step: 200; 120.30.s Loss: 0.9432; AccMul 0.6878 AccBin 0.8273
Step: 300; 180.60.s Loss: 0.9394; AccMul 0.6863 AccBin 0.8256
Step: 400; 240.99.s Loss: 0.9255; AccMul 0.6890 AccBin 0.8269
Step: 500; 301.44.s Loss: 0.9173; AccMul 0.6920 AccBin 0.8278
Step: 600; 361.95.s Loss: 0.9129; AccMul 0.6935 AccBin 0.8289
Step: 700; 422.54.s Loss: 0.9080; AccMul 0.6954 AccBin 0.8294
Step: 800; 483.20.s Loss: 0.9031; AccMul 0.6975 AccBin 0.8300
Step: 900; 543.91.s Loss: 0.9009; AccMul 0.6984 AccBin 0.8302
Step: 1000; 604.64.s Loss: 0.8955; AccMul 0.6997 AccBin 0.8305
Epoch [4/10] Time: 817.31s; BatchTime:0.60s; Loss: 0.8951; MultAcc: 0.7002; BinAcc: 0.8307; 
 	 	 ValLoss: 0.8728; ValMultAcc: 0.7043; ValBinAcc: 0.8381
Count: 25
Step: 100; 60.79.s Loss: 0.8387; AccMul 0.7156 AccBin 0.8388
Step: 200; 121.73.s Loss: 0.8330; AccMul 0.7146 AccBin 0.8402
Step: 300; 182.81.s Loss: 0.8280; AccMul 0.7157 AccBin 0.8398
Step: 400; 244.03.s Loss: 0.8325; AccMul 0.7169 AccBin 0.8412
Step: 500; 305.39.s Loss: 0.8275; AccMul 0.7180 AccBin 0.8409
Step: 600; 366.85.s Loss: 0.8247; AccMul 0.7202 AccBin 0.8422
Step: 700; 428.44.s Loss: 0.8213; AccMul 0.7217 AccBin 0.8435
Step: 800; 490.26.s Loss: 0.8215; AccMul 0.7214 AccBin 0.8431
Step: 900; 552.09.s Loss: 0.8216; AccMul 0.7217 AccBin 0.8429
Step: 1000; 614.06.s Loss: 0.8219; AccMul 0.7216 AccBin 0.8424
Epoch [5/10] Time: 826.51s; BatchTime:0.61s; Loss: 0.8196; MultAcc: 0.7219; BinAcc: 0.8422; 
 	 	 ValLoss: 0.8265; ValMultAcc: 0.7235; ValBinAcc: 0.8460
Count: 30
Step: 100; 61.58.s Loss: 0.7790; AccMul 0.7356 AccBin 0.8513
Step: 200; 123.39.s Loss: 0.7925; AccMul 0.7291 AccBin 0.8480
Step: 300; 185.43.s Loss: 0.7915; AccMul 0.7283 AccBin 0.8467
Step: 400; 247.68.s Loss: 0.7882; AccMul 0.7279 AccBin 0.8457
Step: 500; 310.15.s Loss: 0.7873; AccMul 0.7287 AccBin 0.8462
Step: 600; 372.78.s Loss: 0.7854; AccMul 0.7289 AccBin 0.8450
Step: 700; 435.62.s Loss: 0.7844; AccMul 0.7290 AccBin 0.8460
Step: 800; 498.61.s Loss: 0.7833; AccMul 0.7294 AccBin 0.8460
Step: 900; 561.78.s Loss: 0.7819; AccMul 0.7304 AccBin 0.8469
Step: 1000; 625.14.s Loss: 0.7820; AccMul 0.7302 AccBin 0.8468
Epoch [6/10] Time: 881.32s; BatchTime:0.63s; Loss: 0.7807; MultAcc: 0.7310; BinAcc: 0.8473; 
 	 	 ValLoss: 0.8388; ValMultAcc: 0.7242; ValBinAcc: 0.8436
Count: 35
Step: 100; 62.87.s Loss: 0.6848; AccMul 0.7627 AccBin 0.8613
Step: 200; 126.02.s Loss: 0.6776; AccMul 0.7634 AccBin 0.8617
Step: 300; 189.44.s Loss: 0.6775; AccMul 0.7623 AccBin 0.8618
Step: 400; 253.12.s Loss: 0.6814; AccMul 0.7615 AccBin 0.8615
Step: 500; 317.05.s Loss: 0.6822; AccMul 0.7610 AccBin 0.8618
Step: 600; 381.22.s Loss: 0.6836; AccMul 0.7602 AccBin 0.8612
Step: 700; 445.64.s Loss: 0.6832; AccMul 0.7599 AccBin 0.8613
Step: 800; 510.30.s Loss: 0.6802; AccMul 0.7605 AccBin 0.8612
Step: 900; 575.17.s Loss: 0.6798; AccMul 0.7608 AccBin 0.8616
Step: 1000; 640.24.s Loss: 0.6787; AccMul 0.7607 AccBin 0.8613
Epoch [7/10] Time: 929.88s; BatchTime:0.64s; Loss: 0.6775; MultAcc: 0.7610; BinAcc: 0.8615; 
 	 	 ValLoss: 0.8292; ValMultAcc: 0.7308; ValBinAcc: 0.8461
Count: 40
Step: 100; 64.36.s Loss: 0.5832; AccMul 0.7888 AccBin 0.8723
Step: 200; 128.97.s Loss: 0.5829; AccMul 0.7893 AccBin 0.8715
Step: 300; 193.81.s Loss: 0.5844; AccMul 0.7878 AccBin 0.8725
Step: 400; 258.88.s Loss: 0.5827; AccMul 0.7885 AccBin 0.8730
Step: 500; 324.16.s Loss: 0.5827; AccMul 0.7877 AccBin 0.8727
Step: 600; 389.65.s Loss: 0.5841; AccMul 0.7871 AccBin 0.8726
Step: 700; 455.35.s Loss: 0.5828; AccMul 0.7880 AccBin 0.8732
Step: 800; 521.25.s Loss: 0.5825; AccMul 0.7876 AccBin 0.8733
Step: 900; 587.35.s Loss: 0.5816; AccMul 0.7876 AccBin 0.8737
Step: 1000; 653.64.s Loss: 0.5794; AccMul 0.7883 AccBin 0.8739
Epoch [8/10] Time: 944.80s; BatchTime:0.65s; Loss: 0.5786; MultAcc: 0.7887; BinAcc: 0.8744; 
 	 	 ValLoss: 0.8530; ValMultAcc: 0.7342; ValBinAcc: 0.8498
Count: 45
Step: 100; 65.66.s Loss: 0.5193; AccMul 0.8051 AccBin 0.8803
Step: 200; 131.58.s Loss: 0.5080; AccMul 0.8084 AccBin 0.8825
Step: 300; 197.73.s Loss: 0.5039; AccMul 0.8105 AccBin 0.8843
Step: 400; 264.11.s Loss: 0.5038; AccMul 0.8101 AccBin 0.8839
Step: 500; 330.72.s Loss: 0.5029; AccMul 0.8115 AccBin 0.8853
Step: 600; 397.54.s Loss: 0.5025; AccMul 0.8120 AccBin 0.8857
Step: 700; 464.57.s Loss: 0.5025; AccMul 0.8123 AccBin 0.8862
Step: 800; 531.81.s Loss: 0.5044; AccMul 0.8111 AccBin 0.8858
Step: 900; 599.23.s Loss: 0.5044; AccMul 0.8114 AccBin 0.8861
Step: 1000; 666.85.s Loss: 0.5040; AccMul 0.8116 AccBin 0.8857
Epoch [9/10] Time: 972.22s; BatchTime:0.67s; Loss: 0.5046; MultAcc: 0.8114; BinAcc: 0.8857; 
 	 	 ValLoss: 0.8920; ValMultAcc: 0.7407; ValBinAcc: 0.8495
Count: 50
Step: 100; 66.96.s Loss: 0.4399; AccMul 0.8248 AccBin 0.8922
Step: 200; 134.15.s Loss: 0.4479; AccMul 0.8262 AccBin 0.8932
Step: 300; 201.54.s Loss: 0.4542; AccMul 0.8251 AccBin 0.8936
Step: 400; 269.15.s Loss: 0.4574; AccMul 0.8247 AccBin 0.8931
Step: 500; 336.97.s Loss: 0.4583; AccMul 0.8258 AccBin 0.8937
Step: 600; 404.99.s Loss: 0.4563; AccMul 0.8258 AccBin 0.8930
Step: 700; 473.20.s Loss: 0.4587; AccMul 0.8250 AccBin 0.8924
Step: 800; 541.61.s Loss: 0.4599; AccMul 0.8246 AccBin 0.8917
Step: 900; 610.20.s Loss: 0.4607; AccMul 0.8243 AccBin 0.8912
Step: 1000; 678.96.s Loss: 0.4594; AccMul 0.8248 AccBin 0.8914
Epoch [10/10] Time: 988.32s; BatchTime:0.68s; Loss: 0.4590; MultAcc: 0.8251; BinAcc: 0.8916; 
 	 	 ValLoss: 0.9218; ValMultAcc: 0.7441; ValBinAcc: 0.8511
Count: 55

TEST Loss: 0.9089; AccMult: 0.7409; AccBin: 0.8466

ROC AUC SCORE
	 good: 0.9050
	 celeb: 0.9125
	 casino: 0.8993
	 guns: 0.9165
	 illegal: 0.8678
	 narco: 0.8136
	 porno: 0.9280
	 yellow: 0.8506
Peak memory usage by Pytorch tensors: 2545.60 Mb
