import torch 
from torch import nn
import torchvision


class AdaFilterBlock(nn.Module):
    def __init__(self, cin, cout):
        super(AdaFilterBlock, self).__init__()
        self.global_avgpool = nn.AdaptiveAvgPool2d((1,1))
        self.rnn = nn.LSTM(1, 1)
        self.linear = nn.Linear(cin, cout)
        self.sigmoid = nn.Sigmoid()

    def forward(self, input):
        b, c, _, _ = input.shape
        out = self.global_avgpool(input)
        out, _ = self.rnn(out.view(b, c, 1))
        out = self.sigmoid(self.linear(out.view(b, c)))

        out = (out >= 0.5).long()
        return out


class FilterLayer(nn.Module):
    def __init__(self, cin, cout):
        super(FilterLayer, self).__init__()
        self.filter = AdaFilterBlock(cin, cout)

    def forward(self, input, output):
        f = self.filter(input) 
        b, c = f.shape
        f = f.view(b, c, 1, 1)
        # print((input.shape, output.shape, f.shape))

        output = output * f + output.detach() * (1 - f)
        return output

class FilteredBottleneck(nn.Module):
    def __init__(self, bottleneck):
        super(FilteredBottleneck, self).__init__()
        self.bottleneck = bottleneck
        conv2 = self.bottleneck.conv2
        conv3 = self.bottleneck.conv3
        self.filter1 = FilterLayer(conv2.in_channels, conv2.out_channels)
        self.filter2 = FilterLayer(conv3.in_channels, conv3.out_channels)

    def forward(self, x):
        identity = x 
        bt = self.bottleneck

        out = bt.conv1(x)

        out_base = bt.bn1(out)
        out_base = bt.relu(out_base)
        out_base = bt.conv2(out_base)

        if self.training:
            out = self.filter1(out, out_base)
        else:
            out = out_base

        out_base = bt.bn2(out)
        out_base = bt.relu(out_base)
        out_base = bt.conv3(out_base)

        if self.training:
            out = self.filter2(out, out_base)
        else:
            out = out_base

        if bt.downsample is not None:
            identity = bt.downsample(x)
        
        out += identity
        out = bt.relu(out)

        return out


class FilteredResNet50(nn.Module):
    def __init__(self, classes=8):
        super(FilteredResNet50, self).__init__()
        self.resnet = torchvision.models.resnet50(pretrained=True)

        # self.resnet.layer1 = self._update_layer(self.resnet.layer1)
        # self.resnet.layer2 = self._update_layer(self.resnet.layer2) 
        self.resnet.layer3 = self._update_layer(self.resnet.layer3) 
        self.resnet.layer4 = self._update_layer(self.resnet.layer4)

        in_features = self.resnet.fc.in_features
        self.resnet.fc = torch.nn.Sequential(
            torch.nn.Linear(in_features, classes))
            # torch.nn.Softmax())


    def _update_layer(self, layer):
        layers = []
        for bottleneck in layer.children():
            layers.append(FilteredBottleneck(bottleneck))
        
        return nn.Sequential(*layers)

    def forward(self, x):
        rn = self.resnet

        x = rn.conv1(x)
        x = rn.bn1(x)
        x = rn.relu(x)
        x = rn.maxpool(x)

        x = rn.layer1(x)
        x = rn.layer2(x)
        x = rn.layer3(x)
        x = rn.layer4(x)

        x = rn.avgpool(x)
        x = torch.flatten(x, 1)
        x = rn.fc(x)

        return x
