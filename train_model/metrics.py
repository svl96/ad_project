import torch
import numpy as np
from collections import defaultdict


class Accuracy(torch.nn.Module):
    def __init__(self):
        super(Accuracy, self).__init__()

    def forward(self, outputs, targets):
        _, preds = torch.max(outputs, 1)
        return torch.mean((preds == targets).double())


class AccuracyMult(torch.nn.Module):
    def __init__(self):
        super(AccuracyMult, self).__init__()
    
    def forward(self, outputs, targets, multi=True):
        _, preds = torch.max(outputs, 1)
        assert preds.shape == targets.shape

        if multi:
            return torch.sum((preds == targets)).double() / targets.shape[0]
        
        preds_bin = preds > 0
        targets_bin = targets > 0

        return torch.sum((preds_bin == targets_bin)).double()/ targets.shape[0]


class AccMultWrapper(torch.nn.Module):
    def __init__(self):
        super(AccMultWrapper, self).__init__()
        self.acc_mult = defaultdict(list)
        self.acc_bin = defaultdict(list)
        self.iter_val = defaultdict(int)
        self.metric = AccuracyMult()

        self.hist_mult = defaultdict(list)
        self.hist_bin = defaultdict(list)

    def forward(self, outputs, targets, mode='train'):
        self.acc_mult[mode].append(self.metric(outputs, targets, multi=True).item())
        self.acc_bin[mode].append(self.metric(outputs, targets, multi=False).item())

    def reset(self, mode):
        self.save_hist(mode)
        self.acc_mult[mode] = []
        self.acc_bin[mode] = []

    def save_hist(self, mode):
        self.hist_mult[mode].append(self.get_mean_mult(mode))
        self.hist_bin[mode].append(self.get_mean_bin(mode))

    def get_mean_mult(self, mode):
        return np.mean(self.acc_mult[mode])

    def get_mean_bin(self, mode):
        return np.mean(self.acc_bin[mode])


class DistillationAccuracy(torch.nn.Module):
    def __init__(self):
        super(DistillationAccuracy, self).__init__()

    def forward(self, outputs, targets):
        student, teacher = outputs
        _, preds = torch.max(student, 1)
        return torch.mean((preds == targets).double())
