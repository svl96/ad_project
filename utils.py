import matplotlib.pyplot as plt
from os import listdir, path
from sklearn.model_selection import train_test_split 


def draw_torch_img(img):
    plt.imshow(img.numpy().transpose(1, 2, 0))


def load_data(base_dir='data/toloka_imgs', bad_files=set()):
    # classes = {'good': 0, 'bad/casino': 1,
    # 'bad/celebrity': 2, 'bad/guns': 3,
    # 'bad/illegal': 4, 'bad/narcotics': 5,
    # 'bad/porno': 6, 'bad/yellow': 7}

    classes = ['good_filtered', 'bad_filtered/casino', 'bad_filtered/guns',
                'bad_filtered/illegal', 'bad_filtered/narcotics', 
                'bad_filtered/porno', 'bad_filtered/yellow']

    X = []
    y = []
    for v, key in enumerate(classes):
        for el in listdir(path.join(base_dir, key)):
            img_path = path.join(base_dir, key, el)
            if img_path in bad_files:
                continue
            X.append(img_path)
            y.append(v)
    
    return (X, y)


def split_train_test_val(X, y, test_size=0.1, val_size=0.1, shuffle=True, random_state=42):
    X_train, X_test, y_train, y_test = train_test_split(X, y,
                                    test_size=test_size, shuffle=shuffle, stratify=y, random_state=random_state)

    X_train, X_val, y_train, y_val = train_test_split(X_train, y_train,
                                    test_size=val_size, shuffle=shuffle, stratify=y_train, random_state=random_state)

    return (X_train, y_train), (X_val, y_val), (X_test, y_test)
